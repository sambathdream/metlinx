import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';
import { TextField } from 'redux-form-material-ui';

const required = value => (value === null ? 'Required' : undefined);
let pathname = '/';

class UserAuthForm extends Component {
    componentWillReceiveProps(nextProps) {
        const {location: {state}} = nextProps;

        if (state && state.from && state.from.pathname) {
            pathname = state.from.pathname;
        }
        if (nextProps.success) {
            nextProps.history.push('/');
        }
    }

    render() {
        const {
            error, errorMessage, handleFormSubmit,
            handleSubmit, freeze
        } = this.props;
        return (
            <div>
                <div className="login-wrapper">
                    <div className="login-fields">
                        <h2>Login</h2>
                        <form onSubmit={handleSubmit(handleFormSubmit.bind(this))}>
                            <div>
                                <Field
                                    name="username"
                                    component={TextField}
                                    hintText="Username"
                                    floatingLabelText="Username"
                                    validate={required}
                                />
                            </div>
                            <div>
                                <Field
                                    name="password"
                                    component={TextField}
                                    hintText="Password"
                                    floatingLabelText="Password"
                                    type="password"
                                    validate={required}
                                />
                            </div>
                            <Link to="/forgotpassword" style={{position: 'relative'}}>Forgot Password?</Link>
                            <div className="pt20">
                                <RaisedButton label="Log In" primary={true} fullWidth={true} type="submit" disabled={freeze} />
                            </div>
                            <div className="pt20">
                                <RaisedButton
                                    label="Login with Google"
                                    fullWidth={true}
                                    icon={<span className="googleIcon"></span>}
                                />
                            </div>
                            <div className="pt20">
                                <Link to="/signup">
                                    <RaisedButton label="Not Registered? sign up" fullWidth={true} />
                                </Link>
                            </div>
                        </form>
                    </div>
                </div>
                <Snackbar
                    open={(error || errorMessage) ? true : false}
                    message={error || errorMessage}
                    autoHideDuration={4000}
                />
            </div>
        );
    }
}

export default reduxForm({
    form: 'loginPage',
})(UserAuthForm);
