import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Panel from 'components/Panel';
import RaisedButton from 'material-ui/RaisedButton';
import { TextField } from 'redux-form-material-ui';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import Checkbox from 'material-ui/Checkbox';
import DatePicker from 'material-ui/DatePicker';
import ReactCrop from 'react-image-crop';
import Paper from 'material-ui/Paper';
import FileInput from 'components/formFields/FileInput';
import 'react-image-crop/dist/ReactCrop.css';
import FlatButton from 'material-ui/FlatButton';

const required = value => (value == null ? 'Required' : undefined);

const styles = {
    raisedButton: {
        margin: 12,
    },
    paper: {
        height: 150,
        width: 150,
        textAlign: 'center',
    }
};

class StudentForm extends Component {
    constructor (props) {
        super(props)

        this.state = {
            sendinvite: 0,
            checkboxChecked: false,
            dateOfBirth: {}
        }
    }

    updateCheckbox = () => {
        this.setState({ checkboxChecked: !this.state.checkboxChecked }, function() {
            if(this.state.checkboxChecked === true) {
                this.setState({ sendinvite: 1 })
            } else {
                this.setState({ sendinvite: 0 })
            }
        })
    };

    updateDate = (param1, date) => {
        this.setState({ dateOfBirth: date })
    };

    onImageLoaded = (image) => {
        const cropHeight = (image.width / image.height) * 20;
        this.props.handleCropChange({width: 20, height: cropHeight});
    };

    render() {
        const {
            studentsPicCrop, handleImageUpload,
            studentsPic, handleCropChange, crop,
            file, imageError, openCropper, handleCropperClose,
            handleCropperSubmit, freezeCropper, handleCloseForm, handleSubmit, handleFormSubmit
        } = this.props;
        
        const uploadImage =  (
            <div className="col-md-12" style={{ margin: '0 auto' }}>
                <Paper style={styles.paper} zDepth={5}>
                    <img style={{width: 'inherit'}}  src={`${process.env.REACT_APP_IMAGE_SERVER}${studentsPicCrop}`} alt='profile pic' />
                </Paper>
                {imageError && <p><b>{imageError}</b></p>}
                <RaisedButton
                    containerElement='label'
                    primary={true}
                    label="Upload Image"
                    style={{margin: 10}}
                >
                    <Field
                        component={FileInput}
                        accept=".png,.jpg,.jpeg"
                        onChange={handleImageUpload}
                        name="studentsProfilePic"
                    />
                </RaisedButton>
            </div>
        );

        const imageCropper = (
            <div className="col-md-6" style={{ margin: '0 auto' }}>
                <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                    <ReactCrop
                        src={studentsPic}
                        onChange={handleCropChange}
                        crop={crop}
                        disabled={freezeCropper}
                        onImageLoaded={this.onImageLoaded}
                    />
                </div>
                <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: 5}}>
                    <FlatButton
                        label="Cancel"
                        primary={true}
                        onClick={handleCropperClose}
                        disabled={freezeCropper}
                    />
                    <FlatButton
                        label="Submit"
                        primary={true}
                        keyboardFocused={true}
                        disabled={freezeCropper}
                        onClick={() => handleCropperSubmit(file, crop)}
                    />
                </div>
            </div>
        );
        
        return (
            <Panel title="Create Student">
                <form onSubmit={handleSubmit(handleFormSubmit.bind(this, this.state.sendinvite, this.state.dateOfBirth))}>
                    <div className="column-1" style={{ display: 'flex' }}>
                        <div className="input-group" style={{ flexGrow: 1 }}>
                            <div>
                                <Field  
                                    name="FirstName"
                                    component={TextField}
                                    hintText="First Name"
                                    floatingLabelText="First Name"
                                    validate={required}
                                />
                            </div>
                            <div>
                                <Field
                                    name="lastName"
                                    component={TextField}
                                    hintText="Last Name"
                                    floatingLabelText="Last Name"
                                    validate={required}
                                />
                            </div>
                            <div>
                                <Field
                                    name="studentEMAIL"
                                    component={TextField}
                                    hintText="Email Address"
                                    floatingLabelText="Email Address"
                                    validate={required}
                                />
                            </div>
                        </div>
                        <div className="image-crop" style={{ marginLeft: '5%', flexGrow: 1, marginTop: '57px' }}>
                            { openCropper ? imageCropper : uploadImage }
                        </div>
                    </div>
                    <div className="column-2" style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
                        <div className="gender-select" style={{ display: 'flex', alignItems: 'center' }}>
                            <RadioButtonGroup name="gender" defaultSelected={`${this.props.initialValues && this.props.initialValues.gender}`} style={{ display: 'flex', alignItems: 'center' }}>
                                <RadioButton
                                    value="Male"
                                    label="Male"
                                    inputStyle={{ marginRight: '0' }}
                                />
                                <RadioButton
                                    value="Female"
                                    label="Female"
                                />
                            </RadioButtonGroup>
                        </div>
                    </div>
                    <div className="column-5" style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center' }}>
                        <div className="title" style={{ display: 'flex', alignItems: 'center', width: '12%' }}>
                            <span>Grade: </span>
                        </div>
                        <Field
                            name="gradeCD"
                            component={TextField}
                            hintText="Grade"
                            floatingLabelText="Grade"
                            validate={required}
                            type="number"
                        />
                    </div>
                    <div  className="column-3" style={{ display: 'flex', justifyContent: 'flex-start', borderBottom: '2px solid black' }}>
                        <div className="title" style={{ display: 'flex', alignItems: 'center', width: '20%' }}>
                            <span>Date of Birth: </span>
                        </div>
                        <div className="date-picker">
                            <DatePicker
                                hintText="DD/MM/YY"
                                onChange={this.updateDate}
                            />
                        </div>
                    </div>
                    <div className="column-4" style={{ marginTop: '2%' }}>
                        <div className="title" style={{ display: 'flex', justifyContent: 'space-between' }} >
                            <div>
                                <span style={{ fontWeight: '500' }}>Parent/Guardian Details</span>
                            </div>
                            <div style={{ width: '37%', display: 'flex', justifyContent: 'flex-start' }}>
                                <Checkbox
                                    label="Send welcome invitation"
                                    labelPosition="left"
                                    checked={this.state.checkboxChecked}
                                    onCheck={() => this.updateCheckbox()}
                                />
                            </div>
                        </div>
                        <div className="top" style={{ display: 'flex', justifyContent: 'space-between'  }}>
                            <div style={{width: '50%'}}>
                                <Field
                                    name="parentFirstName"
                                    component={TextField}
                                    hintText="FirstName"
                                    floatingLabelText="FirstName"
                                    validate={required}
                                />
                            </div>
                            <div style={{width: '50%'}}>
                                <Field
                                    name="parentLastName"
                                    component={TextField}
                                    hintText="LastName"
                                    floatingLabelText="LastName"
                                    validate={required}
                                />
                            </div>
                        </div>
                        <div className="bottom" style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <div style={{width: '50%'}}>
                                <Field
                                    name="parentEMAIL"
                                    component={TextField}
                                    hintText="Email Address"
                                    floatingLabelText="Email Address"
                                    validate={required}
                                />
                            </div>
                            <div style={{width: '50%'}}>
                                <Field
                                    name="parentMobilePhone"
                                    component={TextField}
                                    hintText="Contact Cell:"
                                    floatingLabelText="Contact Cell:"
                                    validate={required}
                                />
                            </div>
                        </div>
                    </div>
                    <div style={{ marginTop: '8%' }}>
                        <RaisedButton label="Submit" primary={true} type="submit" />
                        <RaisedButton
                            style={styles.raisedButton}
                            label="Cancel"
                            primary={true}
                            onClick={() =>handleCloseForm()}
                        />
                    </div>
                </form>
            </Panel>
        )
    }
}

export default reduxForm({
    form: 'studentForm',
    enableReinitialize: true
})(StudentForm);
