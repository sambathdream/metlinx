import React, { Component } from 'react';
import Panel from '../Panel.js';
import RaisedButton from 'material-ui/RaisedButton';
import EditorPen from 'material-ui/svg-icons/editor/mode-edit';
import FlatButton from 'material-ui/FlatButton';
import Drawer from 'material-ui/Drawer';
import StudentCreate from 'components/students/StudentForm';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import {fullWhite} from "material-ui/styles/colors";

class StudentsPage extends Component {
    constructor (props) {
        super(props)

        this.state = {
        }
    }

    componentDidMount() {
        this.props.getStutdentList()
    }

    render () {
        const { studentLists, toggleStudentForm, openStudentForm, formOpen } = this.props;

        return (
            <Panel title="Student Information">
                {
                    formOpen &&
                    <Drawer width={'50%'} openSecondary={true} open={true}>
                        <StudentCreate {...this.props} initialValues={this.props.initialForm} />
                    </Drawer>
                }
                <div className="add-student-btn">
                    <RaisedButton
                        label="Add student"
                        backgroundColor="#a4c639"
                        labelColor={fullWhite}
                        onClick={() => openStudentForm(true)}
                    />
                </div>
                <Table>
                    <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn>#</TableHeaderColumn>
                            <TableHeaderColumn>First Name</TableHeaderColumn>
                            <TableHeaderColumn>Last Name</TableHeaderColumn>
                            <TableHeaderColumn>GradeLevel</TableHeaderColumn>
                            <TableHeaderColumn></TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {
                            Object.keys(studentLists).map((key, index) => {
                                return (
                                    <TableRow key={index}>
                                        <TableRowColumn>{index + 1}</TableRowColumn>
                                        <TableRowColumn>{studentLists[key].FirstName}</TableRowColumn>
                                        <TableRowColumn>{studentLists[key].lastName}</TableRowColumn>
                                        <TableRowColumn>{studentLists[key].gradeCD}</TableRowColumn>
                                        <TableRowColumn>
                                            <FlatButton
                                                icon={<EditorPen />}
                                                className="inline-btn"
                                                onClick={() => toggleStudentForm(studentLists[key].studentID, true)}
                                            />
                                            {/*{*/}
                                                {/*!studentLists[key].standardFlag && (*/}
                                                    {/*<FlatButton*/}
                                                        {/*icon={<ActionDelete />}*/}
                                                        {/*className="inline-btn"*/}
                                                        {/*onClick={() => onProfileDelete(profiles[key].profileID)}*/}
                                                    {/*/>*/}
                                                {/*)*/}
                                            {/*}*/}
                                        </TableRowColumn>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody>
                </Table>
            </Panel>
        )
    }
}

export default StudentsPage