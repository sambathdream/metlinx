import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import NotificationWrapper from './NotificationWrapper';
import CircularProgress from 'material-ui/CircularProgress';

const PrivateRoute = ({ component: Component, loggedIn, ...rest }) => (
  <Route {...rest} render={props => {
    return (
      (!loggedIn) ? (
        <Redirect to={{
            pathname: '/admin',
            state: { from: props.location }
          }}
        />
      ) : (rest.flowStatus < 800 && rest.path.toLowerCase() === '/signupflow') ? (
        <Component {...props}/>
      ) : (rest.flowStatus < 800) ? (
        <Redirect to={{ pathname: '/signupflow' }} />
      ) : (!rest.flowStatus) ? (
        <NotificationWrapper
          materialIcon={<CircularProgress size={80} thickness={5} />}
          notification="Loading...."
        />
      ) : (
        <Component {...props}/>
      )
    )
  }}/>
);

export default PrivateRoute;
