import React from 'react';

const NotificationWrapper = ({notification, materialIcon}) => (
  <div className="login-wrapper">
    <div className="login-fields text-center">
      <h3>{materialIcon}</h3>
      <div className="pt20">
        <p>{notification}</p>
      </div>
    </div>
  </div>
);

export default NotificationWrapper;
