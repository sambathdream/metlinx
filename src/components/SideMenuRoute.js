import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import NotificationWrapper from './NotificationWrapper';
import CircularProgress from 'material-ui/CircularProgress';


const SideMenuRoute = ({ component: Component, loggedIn, ...rest }) => (
    <   Route {...rest} render={props => {
        return (

                (!loggedIn) ? (<Redirect to={{pathname: '/admin', state: { from: props.location }}} />)
              : (rest.flowStatus < 800 && rest.path.toLowerCase() === '/') ? (<Component {...props}/>)
              : (rest.flowStatus < 800) ? (<Redirect to={{ pathname: '/' }} />)
              : (!rest.flowStatus) ? (<NotificationWrapper materialIcon={<CircularProgress size={80} thickness={5} />} notification="Loading...."/>)
              : (<Component {...props}/> )
        )
    }}/>
);

export default SideMenuRoute;
