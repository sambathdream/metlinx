import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import Snackbar from 'material-ui/Snackbar';
import RaisedButton from 'material-ui/RaisedButton';
import { TextField } from 'redux-form-material-ui';

const required = value => (value === null ? 'Required' : undefined);
const email = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email'
    : undefined);

class ForgotPasswordPage extends Component {
  render() {
    const {error, errorMessage, handleFormSubmit, handleSubmit, freeze, success} = this.props;
    const resetPassword = (
      <div>
        <p>Enter your email address and we will send you a link to reset your password.</p>
        <form onSubmit={handleSubmit(handleFormSubmit.bind(this))}>
          <div>
            <Field
              name="userEMAIL"
              component={TextField}
              floatingLabelText="Enter your email address"
              hintText="Email"
              validate={[required, email]}
            />
          </div>
          <div className="pt20">
            <RaisedButton label="Submit" primary={true} fullWidth={true} type="submit" disabled={freeze} />
          </div>
        </form>
      </div>
    );

    const checkEmail = (
      <div>
        <h3><i className="material-icons">email</i></h3>
        <div className="pt20">
          <p>If you have a valid registration, you will receive email to reset your password. Even if you have registered and not received your reset password link in next 1 hour, Pease contact the classteams support.</p>
        </div>
      </div>
    );

    return (
      <div className="login-wrapper">
        <div className="login-fields text-center">
          <h2>Reset Password</h2>
          {success ? checkEmail : resetPassword}
        </div>
        <Snackbar
          open={(error || errorMessage) ? true : false}
          message={error || errorMessage}
          autoHideDuration={4000}
        />
      </div>
    );
  }
}

export default reduxForm({
  form: 'forgotPasswordPage',
})(ForgotPasswordPage);
