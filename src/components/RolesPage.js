import React, { Component } from 'react';
import Panel from './Panel.js';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

class RolesPage extends Component {
  componentDidMount() {
    let activeValue = Object.values(this.props.menuList && Object.values(this.props.menuList).map(item => Object.values(item).indexOf('Roles'))).find(function (item) {
      return item > -1;
    });
    if (!this.props.roles.length && (activeValue !== undefined)) {
      this.props.fetchRoles();
    } else if (activeValue === undefined) {
      this.props.history.push('/undefined');
    }
  }

  render() {
    return (
      <Panel title="User Approval">
        <Table>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>#</TableHeaderColumn>
              <TableHeaderColumn>Role ID</TableHeaderColumn>
              <TableHeaderColumn>Role Desc</TableHeaderColumn>
              <TableHeaderColumn>Role Image</TableHeaderColumn>
              <TableHeaderColumn>Role Level</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
              {
                this.props.roles.map((role, index) => {
                  return (
                    <TableRow key={role.id}>
                      <TableRowColumn>{index + 1}</TableRowColumn>
                      <TableRowColumn>{role.roleID}</TableRowColumn>
                      <TableRowColumn>{role.roledesc}</TableRowColumn>
                      <TableRowColumn>{role.roleIDimage}</TableRowColumn>
                      <TableRowColumn>{role.roleLevel}</TableRowColumn>
                    </TableRow>
                  );
                })
              }
          </TableBody>
        </Table>
      </Panel>
    )
  }
}

export default RolesPage;
