import React, {Component} from 'react';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import SignUpProgress from 'containers/signUpFlow/SignUpProgress';
import {Link, withRouter} from 'react-router-dom';

class AvatarDropdown extends Component {
    state = {
        valueSingle: '3',
        valueMultiple: ['3', '5'],
        openMenu: false
    };

    handleOnRequestChange = (value) => {
        this.setState({
            openMenu: value,
        });
    };

    componentDidMount() {
        this.forceUpdate();
    }

    handleMoveToLoginPage = () => {
        window.location.href = '/login'
    };
    clearLocalStorage = () => {
        localStorage.removeItem('userEMAIL');
        localStorage.removeItem('role');
        localStorage.removeItem('mxauthorization');
        localStorage.removeItem('email');
        localStorage.removeItem('schoolLevel');
        localStorage.removeItem('userROLE');
        localStorage.removeItem('schoolID');
        localStorage.removeItem('flowStatus');
    };

    render() {
        const style = {
            zIndex: '1500'
        };

        const {loggedIn, handleLogout, flowStatus, logoutMessage} = this.props;
        return (
            <div className="header-container">
                <header className="an-header">
                    {
                        loggedIn && (
                            <div className="header-right">
                                <IconMenu
                                    style={style}
                                    anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                                    // ???? Check on the following URL
                                    iconButtonElement={
                                        <IconButton>
                                            <Avatar
                                                src="http://simplycodex.com/wp-content/uploads/2017/11/default-gravatar.png"
                                                size={35}
                                            />
                                        </IconButton>
                                    }
                                    open={this.state.openMenu}
                                    onRequestChange={this.handleOnRequestChange}
                                >
                                    <MenuItem primaryText="Teacher Profile" containerElement={<Link to="/teacherprofile"/>}
                                              leftIcon={<i className="material-icons">account_circle</i>}/>
                                    <MenuItem primaryText="Notification"
                                              leftIcon={<i className="material-icons">mail_outline</i>}/>
                                    <MenuItem primaryText="Calendar"
                                              leftIcon={<i className="material-icons">date_range</i>}/>
                                    <Divider/>
                                    <MenuItem
                                        primaryText="Sign Out"
                                        leftIcon={<i className="material-icons">power_settings_new</i>}
                                        onClick={handleLogout}
                                    />
                                </IconMenu>
                            </div>
                        )
                    }
                    {
                        (!loggedIn && (this.props.history.location.pathname === '/admin')) === true ?
                            <RaisedButton primary={true} onClick={() => this.handleMoveToLoginPage()} label="Admin Login"/> :
                            null
                    }
                    {
                        flowStatus < 800 && (
                            <SignUpProgress/>
                        )
                    }
                </header>
                {
                    logoutMessage && <div className="alert-message-wrapper" style={{marginBottom: '70%'}}>
                        <div className="alert-message"><span>Thank you for using classteams!</span></div>
                    </div>
                }
            </div>
        );
    }
}

export default withRouter(AvatarDropdown)
