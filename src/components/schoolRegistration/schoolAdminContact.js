import React from 'react'
import { Field, reduxForm } from 'redux-form';
import { TextField } from 'redux-form-material-ui';
import {Grid , Button } from '@material-ui/core';
import '../../static/css/component-form.css';


const required = value => (value == null ? 'Required' : undefined);
const email = value =>
    (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Invalid email'
        : undefined);

const SchoolAdminContact = props => {
    const { handleSubmit , previousPage} = props;
    return (
        <form onSubmit={handleSubmit}>

            <div>
                <Grid container>
                    <Grid item sm>
                        <Field
                        name="firstName"
                        component={TextField}
                        hintText="First Name"
                        floatingLabelText="First Name"
                        validate={required}
                        />
                    </Grid>


                    <Grid item sm>
                        <Field
                        name="lastName"
                        component={TextField}
                        hintText="Last Name"
                        floatingLabelText="Last Name"
                        validate={required}
                        />
                    </Grid>
                </Grid>
            </div>

            <div >
                <Grid container>
                    <Grid item sm>
                    <Field
                        name="email"
                        component={TextField}
                        hintText="Email"
                        floatingLabelText="Email"
                        validate={[required, email]}
                    />
                    </Grid>
                    <Grid item sm>
                    <Field
                        name="confirmEmail"
                        component={TextField}
                        hintText="Confirm Email"
                        floatingLabelText="Confirm Email"
                        validate={[required, email]}
                    />
                    </Grid>
                </Grid>
            </div>

            <div >
                <Grid container>
                <Grid item sm>
                  <Field name="password"  component={TextField}  hintText="Password" floatingLabelText="Password" type="password" validate={required}/>
                </Grid>
                <Grid item sm>
                  <Field name="confirmPassword" component={TextField} hintText="Confirm Password" floatingLabelText="Confirm Password" type="password"  validate={required}/>
                </Grid>
                </Grid>
            </div>

            <div className="form-footer" align="center" >
               <Grid container>
                   <Grid item sm>

                       <Button  type="button"
                                style={{margin:10}}
                                variant="raised"
                                size="medium"
                                color="primary"
                                className="previous btn-lg"
                                onClick={previousPage}>
                           { "< Previous"}
                       </Button>

                    <Button
                         variant="raised"
                        size="medium"

                        color="primary"
                        type="submit"
                    >
                        { "Next >" }
                   </Button>

                   </Grid>
               </Grid>

            </div>
        </form>
    )
}

export default reduxForm({
    form: 'schoolRegistrationForm', // <------ same form name
    destroyOnUnmount: false, // <------ preserve form data
    forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(SchoolAdminContact)

