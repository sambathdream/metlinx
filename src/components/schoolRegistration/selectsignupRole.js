import React, {Component} from 'react';
import {Grid, Button } from '@material-ui/core';
import student from '../../assets/student.jpg';
import parent from '../../assets/parent.jpg';
import teacher from '../../assets/teacher.jpg';
import {reduxForm} from "redux-form";
import '../../static/css/component-form.css';

const contentStyle = {
    marginTop: '5%',
};

const itemStyle = {
    width: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    maxHeight: '100%',
    alignItems: 'center',
    margin: '7%',
    cursor: 'pointer',
};
const imageStyle = itemStyle;
const studentStyle = {
    width: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    maxHeight: '40%',
    alignItems: 'center',
    margin: '7%',
    cursor: 'pointer',
    outline: '2px red'
};

const letterStyle = {
    fontSize: '1rem',
    fontWeight: '100',
    color: 'black',
    textShadow: '3px 4px 7px rgba(187, 222, 217, 1)'
};
const clearMyLocalStorage = () =>{
    localStorage.removeItem('userEMAIL');
    localStorage.removeItem('role');
    localStorage.removeItem('mxauthorization');
    localStorage.removeItem('email');
    localStorage.removeItem('schoolLevel');
    localStorage.removeItem('userROLE');
    localStorage.removeItem('schoolID');
    localStorage.removeItem('flowStatus');
    return true;
};
class  SignupRole extends Component {

    state = {
        isRoleselected:false,
        isTeacherSelected:false,
        isStudentSelected:false,
        isParentSelected:false

    }

    parentClick = (e) => {
        if (this.state.isParentSelected) {
            this.setState({isParentSelected:!this.state.isParentSelected});
            this.setState({isTeacherSelected:false});
            this.setState({isStudentSelected:false});
        } else {
            this.setState({isParentSelected:!this.state.isParentSelected});
            this.setState({isTeacherSelected:false});
            this.setState({isStudentSelected:false});

        }

        localStorage.setItem('roleParent',this.state.isParentSelected )
    };

    studentClick = (e) => {
        if (this.state.isStudentSelected) {
            this.setState({isParentSelected:false});
            this.setState({isTeacherSelected:false});
            this.setState({isStudentSelected:!this.state.isStudentSelected});
        } else {
            this.setState({isParentSelected: false});
            this.setState({isTeacherSelected:false});
            this.setState({isStudentSelected:!this.state.isStudentSelected});

        }


        localStorage.setItem('role','STUDENT')
    };
    teacherClick = (e) => {
        if (this.state.isTeacherSelected) {
            this.setState({isParentSelected:false});
            this.setState({isTeacherSelected:!this.state.isTeacherSelected});
            this.setState({isStudentSelected:false});
        } else {
            this.setState({isParentSelected: false});
            this.setState({isTeacherSelected:!this.state.isTeacherSelected});
            this.setState({isStudentSelected:false});

        }

        localStorage.setItem('role','TEACHER')
    };

    render() {
        const {handleSubmit} = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <div>
                    <Grid container style={contentStyle}>
                        <Grid item xs>
                            <div className={this.state.isStudentSelected ? "student-clicked" : "student"} style={imageStyle}
                                 onClick={(e) => this.studentClick(e)}>

                                <img src={student}/>
                                <span style={letterStyle}>Student</span>
                            </div>
                        </Grid>
                        <Grid item xs>
                            <div className={this.state.isParentSelected ? "parent-clicked" :  "parent"} style={imageStyle}
                                 onClick={(e) => this.parentClick(e)}>
                                <img src={parent}/>
                                <span style={letterStyle}>Parent</span>
                            </div>
                        </Grid>
                        <Grid item xs>
                            <div className={this.state.isTeacherSelected ? "teacher-clicked" : "teacher"} style={imageStyle}
                                 onClick={(e) => this.teacherClick(e)}>
                                <img src={teacher}/>
                                <span style={letterStyle}>Teacher</span>
                            </div>
                        </Grid>
                    </Grid>
                </div>

                <div className="form-footer" align="center">
                    <Grid container>
                        <Grid item sm>

                            <Button
                                variant="raised"
                                size="medium"

                                color="primary"
                                type="submit"
                            >
                                Next >
                            </Button>

                        </Grid>
                    </Grid>

                </div>
            </form>

        )
    }
};

export default reduxForm({
    form: 'schoolRegistrationForm', //Form name is same
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(SignupRole)
