import React from 'react'
import { Field, reduxForm } from 'redux-form'
import {SelectField } from 'redux-form-material-ui';
import {Grid, Button } from '@material-ui/core';
import MenuItem from 'material-ui/MenuItem';

const required = value => (value == null ? 'Required' : undefined);

const SchoolGeoLocation = (props) => {

     const {
        handleSubmit,onSubmit, previousPage, stateChanged,
        stateList, districtList, countyList, freeze
    } = props;

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div align="center">
                <div>

                    <Field
                        name="stateCD"
                        component={SelectField}
                        hintText="select your State here"
                        floatingLabelText="select your State here"
                        validate={required}
                        onChange={stateChanged}
                    >
                        {
                            stateList.map((mystate, index) => {
                                return (
                                    <MenuItem key={index} value={mystate.stateCD} primaryText={mystate.statename} />
                                );
                            })
                        }
                    </Field>
                </div>
                <div>
                    <Field
                        name="districtCD"
                        component={SelectField}
                        hintText="School District"
                        floatingLabelText="School District"
                        validate={required}
                    >
                        {
                            districtList.map((district, index) => {
                                return (
                                    <MenuItem key={index} value={district.districtCD} primaryText={district.districtName} />
                                );
                            })
                        }
                    </Field>
                </div>
                <div>
                    <Field
                        name="countyCD"
                        component={SelectField}
                        hintText="School County"
                        floatingLabelText="School County"
                        validate={required}
                    >
                        {
                            countyList.map((county, index) => {
                                return (
                                    <MenuItem key={index} value={county.countyCD} primaryText={county.countyName} />
                                );
                            })
                        }
                    </Field>
                </div>
            </div>
            <div className="form-footer" align="center">
                <Grid container>
                <Grid item sm>
                <Button  type="button"
                         style={{margin:10}}
                         variant="raised"
                         size="medium"
                         color="primary"
                         className="previous btn-lg"
                         onClick={previousPage}>
                    Previous
                </Button>

                <Button type="submit"
                        variant="raised"
                        style={{margin:10}}
                        size="medium"
                        color="primary"
                        className="btn-lg"
                        disabled={freeze}>
                    Submit
                </Button>
                </Grid>
                </Grid>
            </div>
        </form>
    )
}

export default reduxForm({
    form: 'schoolRegistrationForm', //Form name is same
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true // <------ unregister fields on unmount
})(SchoolGeoLocation)