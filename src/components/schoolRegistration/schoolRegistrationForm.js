import React, { Component } from 'react'
import PropTypes from 'prop-types'
import SignupRole from './selectsignupRole';
import SchoolAdminContact from './schoolAdminContact'
import SchoolGeoLocation from './schoolGeoLocation'
import FormPanel from "../FormPanel";


class SchoolRegistrationForm extends Component {
    constructor(props) {
        super(props);
        this.nextPage = this.nextPage.bind(this);
        this.previousPage = this.previousPage.bind(this);
        this.state = {
            page: 1
        }

    }


    componentDidMount() {

        if (this.props.states.length === 0) {
            this.props.getAllStates();
            localStorage.setItem('roleTEACHER',false);
            localStorage.setItem('roleSTUDENT',false);
            localStorage.setItem('rolePARENT',false);
        }
    }
    nextPage() {
        this.setState({ page: this.state.page + 1 })
    }


    previousPage() {
        this.setState({ page: this.state.page - 1 })
    }

    render() {
        const { handleFormSubmit} = this.props;

        const { page } = this.state;
        const { error, errorMessage} = this.props;
        return (
            <FormPanel title="New School Registration" styles={{width: '40%'}}>
                {error && <strong>{error}</strong>}
                {errorMessage && <strong>{errorMessage}</strong>}
                <div>
                    {page === 1 &&  <SignupRole onSubmit={this.nextPage} />}
                    {page === 2 &&  <SchoolAdminContact onSubmit={this.nextPage}  previousPage={this.previousPage} />}
                    {page === 3 && (
                        <SchoolGeoLocation
                        stateList = {this.props.states}
                        stateChanged = {this.props.handleStateChange}
                        districtList = {this.props.districts}
                        countyList = {this.props.counties}
                        previousPage={this.previousPage}
                        onSubmit={handleFormSubmit} />
                    )}
                </div>
            </FormPanel>
        )
    }
}

SchoolRegistrationForm.propTypes = {
    handleFormSubmit: PropTypes.func.isRequired
}

export default SchoolRegistrationForm