import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import student from '../assets/student.jpg';
import parent from '../assets/parent.jpg';
import teacher from '../assets/teacher.jpg';

const adminStyle = {
};

const contentStyle = {
    display: 'flex',
    justifyContent: 'center',
    width: '60%',
    height: 'auto',
    margin: '0 auto',
    minHeight: '100%',
    boxShadow: '2px 2px 66px 0px rgba(0,0,0,0.75)'
};

const itemStyle = {
    width: '30%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    margin: '8%',
    cursor: 'pointer'
};

const letterStyle = {
    fontSize: '1rem',
    fontWeight: '200',
    color: 'black',
    textShadow: '3px 4px 7px rgba(187, 222, 217, 1)'
};
const clearMyLocalStorage = () =>{
    localStorage.removeItem('userEMAIL');
    localStorage.removeItem('role');
    localStorage.removeItem('mxauthorization');
    localStorage.removeItem('email');
    localStorage.removeItem('schoolLevel');
    localStorage.removeItem('userROLE');
    localStorage.removeItem('schoolID');
    localStorage.removeItem('flowStatus');
    return true;
};


class AdminPage extends Component {
    constructor(props) {

        super(props)
        clearMyLocalStorage();
    }

    studentClick = () => {


            localStorage.setItem('role','STUDENT')
            window.location.href = '/login'

    };
    teacherClick = () => {


        localStorage.setItem('role','TEACHER')
        window.location.href = '/login'

    };
    parentClick = () => {


        localStorage.setItem('role','PARENT')
        window.location.href = '/login'

    };
    adminClick = () => {


        localStorage.setItem('role','ADMIN')
        window.location.href = '/login'

    };
    render() {
        return (


            <div className="admin-container">
                 <div className="content" style={contentStyle}>
                    <div className="student" style={itemStyle} onClick={this.studentClick}>
                        <img src={student} />
                        <span style={letterStyle}>Student</span>
                    </div>
                    <div className="parent" style={itemStyle} onClick={this.parentClick}>
                        <img src={parent} />
                        <span style={letterStyle}>Parent</span>
                    </div>
                    <div className="teacher" style={itemStyle} onClick={this.teacherClick}>
                        <img src={teacher} />
                        <span style={letterStyle}>Teacher</span>
                    </div>
                 </div>
            </div>

        )
    }
}

export default withRouter(AdminPage);