import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Panel from 'components/Panel';
import RaisedButton from 'material-ui/RaisedButton';
import { TextField } from 'redux-form-material-ui';
import CheckboxGroup from 'components/formFields/CheckboxGroup';

const required = value => (value == null ? 'Required' : undefined);

const styles = {
  raisedButton: {
    margin: 12,
  }
};

class ProfileForm extends Component {
  componentDidMount() {
    if (!this.props.roles.length) {
      this.props.fetchRoles();
    }
    if (!this.props.features.length) {
      this.props.fetchFeatures();
    }
  }

  render() {
    const {
      toggleProfileForm, handleSubmit, error, handleFormSubmit, profileID,
      roles, features, errorMessage, freeze,
    } = this.props;
    const filteredRoles = roles.map(role => {
      return { label: role.roleID, value: role.roleID };
    });
    const featureOptions = {};
    features.forEach(feature => {
      if (featureOptions[feature.moduleID]) {
        featureOptions[feature.moduleID].push({
          label: feature.featureName,
          value: feature.featureID,
        });
      } else {
        featureOptions[feature.moduleID] = [{
          label: feature.featureName,
          value: feature.featureID,
        }];
      }
    });

    return (
      <Panel title={profileID === 'new' ? "Create Profile" : "Edit Profile"}>
        {error && <strong>{error}</strong>}
        {errorMessage && <strong>{errorMessage}</strong>}
        <form onSubmit={handleSubmit(handleFormSubmit.bind(this))}>
          <div>
            <Field
              name="profileName"
              component={TextField}
              hintText="Profile Name"
              floatingLabelText="Profile Name"
              validate={required}
            />
          </div>
          <div>
            <Field
              name="profileDesc"
              component={TextField}
              hintText="Profile Description"
              floatingLabelText="Profile Description"
              validate={required}
            />
          </div>
          <div style={{marginTop: 15}}>
            <Field
              name="roles"
              component={CheckboxGroup}
              label="Roles"
              options={filteredRoles}
            />
          </div>
          <div style={{marginTop: 15}}>Modules Features</div>
          {
            Object.keys(featureOptions).map((key, index) => {
              return (
                <div key={index} className="ml-3">
                  <Field
                    name={`modules[${key}]`}
                    label={key}
                    options={featureOptions[key]}
                    component={CheckboxGroup}
                    validate={required}
                  />
                </div>
              )
            })
          }
          <div>
            <RaisedButton label="Submit" primary={true} style={styles.raisedButton} type="submit" disabled={freeze} />
            <RaisedButton
              style={styles.raisedButton}
              label="Cancel"
              primary={true}
              onClick={() => toggleProfileForm(profileID, false)}
            />
          </div>
        </form>
      </Panel>
    )
  }
}

export default reduxForm({
  form: 'profileForm',
  enableReinitialize: true,
})(ProfileForm);
