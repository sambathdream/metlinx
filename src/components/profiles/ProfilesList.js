import React, { Component } from 'react';
import Panel from 'components/Panel.js';
import ProfileFilter from './ProfileFilter';
import ProfileCreate from 'containers/profiles/ProfileCreate.js';
import FlatButton from 'material-ui/FlatButton';
import ActionDelete from 'material-ui/svg-icons/action/delete-forever';
import EditorPen from 'material-ui/svg-icons/editor/mode-edit';
import {fullWhite} from 'material-ui/styles/colors';
import Drawer from 'material-ui/Drawer';
import RaisedButton from 'material-ui/RaisedButton';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

class ProfilesList extends Component {
  componentDidMount() {
    let activeValue = Object.values(this.props.menuList && Object.values(this.props.menuList).map(item => Object.values(item).indexOf('profiles'))).find(function (item) {
      return item > -1;
    });
    if (!this.props.profiles.length && (activeValue !== undefined)) {
      this.props.fetchProfiles();
    } else if (activeValue === undefined) {
      this.props.history.push('/undefined');
    }
  }

  getModulesFeatures = (modules) => {
    const featuresNormalized = {};
    this.props.features.forEach(feature => {
      featuresNormalized[feature.featureID] = feature.featureName;
    })
    return Object.keys(modules).map((module, index) => {
      return (
        <p key={`${module}-${index}`}>
          {module}: {modules[module].map(featureID => {
            return featuresNormalized[featureID];
          }).join(', ')}
        </p>
      )
    })
  }

  render() {
    const {
      profiles, form, toggleProfileForm, onProfileDelete, filter, onProfileFilterChange, onInputChange,
    } = this.props;
    return (
      <Panel title="Profiles">
        <Drawer width={'50%'} openSecondary={true} open={form.open}>
          {
            form.open ? <ProfileCreate /> : null
          }
        </Drawer>
        <div className="row">
          <div className="col-sm-9">
            <ProfileFilter
              initialValues={filter}
              onFilterChange={onProfileFilterChange}
              onInputChange={onInputChange}
            />
          </div>
          <div className="col-sm-3">
            <RaisedButton
              label="Add New Profile"
              backgroundColor="#a4c639"
              labelColor={fullWhite}
              onClick={() => toggleProfileForm('new', true)}
            />
          </div>
        </div>
        <Table>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>#</TableHeaderColumn>
              <TableHeaderColumn>Profile</TableHeaderColumn>
              <TableHeaderColumn>Profile Description</TableHeaderColumn>
              <TableHeaderColumn>Modules with Features</TableHeaderColumn>
              <TableHeaderColumn>Actions</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              Object.keys(profiles).map((key, index) => {
                return (
                  <TableRow key={index}>
                    <TableRowColumn>{index + 1}</TableRowColumn>
                    <TableRowColumn>{profiles[key].profileName}</TableRowColumn>
                    <TableRowColumn>{profiles[key].profileDesc}</TableRowColumn>
                    <TableRowColumn>
                      {
                        profiles[key]['modules'] && this.getModulesFeatures(profiles[key]['modules'])
                      }
                    </TableRowColumn>
                    <TableRowColumn>
                      <FlatButton
                        icon={<EditorPen />}
                        className="inline-btn"
                        onClick={() => toggleProfileForm(profiles[key].profileID, true)}
                      />
                      {
                        !profiles[key].standardFlag && (
                          <FlatButton
                            icon={<ActionDelete />}
                            className="inline-btn"
                            onClick={() => onProfileDelete(profiles[key].profileID)}
                          />
                        )
                      }
                    </TableRowColumn>
                  </TableRow>
                );
              })
            }
          </TableBody>
        </Table>
      </Panel>
    )
  }
}

export default ProfilesList;
