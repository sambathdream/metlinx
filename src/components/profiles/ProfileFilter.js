import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Checkbox, TextField } from 'redux-form-material-ui';

class ProfileFilter extends Component {
  render() {
    const { onFilterChange, onInputChange, initialValues } = this.props;
    return (
      <form>
        <div className="row">
          <div className="col-sm-3" style={{marginTop: 12}}>
            <Field
              name="enabled"
              component={Checkbox}
              label="Filter"
              onCheck={onFilterChange}
              checked={initialValues.enabled}
            />
          </div>
          <div className="col-sm-9">
            <Field
              name="filterText"
              component={TextField}
              label="Filter Profiles"
              value={initialValues.filterText}
              onChange={onInputChange}
            />
          </div>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'profileFilter',
  enableReinitialize: true,
})(ProfileFilter);
