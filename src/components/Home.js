import React, { Component } from 'react';

class Home extends Component {
  constructor (props) {
    super(props);

    this.state = {
      errorAlert: this.props.menuList
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      errorAlert: nextProps.menuList
    })
  }

  render () {
    return(
      <div className="home-page-welcome-alert">
        <span className="first-line">Welcome to CLASSTEAMS!</span>
        { this.state.errorAlert && (this.state.errorAlert.status === 0 ? <span className="second-line">Please contact your school Admin for your Menu options</span> : null) }
      </div>
    )
  }
}

export default Home;
