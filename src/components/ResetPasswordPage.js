import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Snackbar from 'material-ui/Snackbar';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import { TextField } from 'redux-form-material-ui';
import { Link, withRouter } from 'react-router-dom';
import ActionHome from 'material-ui/svg-icons/action/home';
import qs from 'qs';

let defEMAIL = null;

const required = value => (value == null ? 'Required' : undefined);
class ResetPasswordPage extends Component {
    componentDidMount() {
        const qsParsed = qs.parse(this.props.location.search.slice(1));
        defEMAIL = qsParsed.userEMAIL;
        localStorage.setItem('userEMAIL',defEMAIL);
    }

    render() {


    const {error, errorMessage, handleFormSubmit, handleSubmit, freeze, success} = this.props;

    return (
      <div className="login-wrapper">
        <div className="login-fields text-center">
          <h2>Change Password</h2>
          <form onSubmit={handleSubmit(handleFormSubmit.bind(this))}>

            <div>
              <Field
                name="password"
                component={TextField}
                floatingLabelText="Password"
                hintText="Password"
                validate={required}
                type="password"
              />
            </div>
            <div>
              <Field
                name="confirmPassword"
                component={TextField}
                floatingLabelText="Confirm Password"
                hintText="Confirm Password"
                validate={required}
                type="password"
              />
            </div>
            <div className="pt20">
              <RaisedButton label="Change Password" primary={true} fullWidth={true} type="submit" disabled={freeze} />
            </div>
            <div className="pt20">
              <FlatButton
                label="Go To Home"
                icon={<ActionHome />}
                fullWidth={true}
                containerElement={<Link to="/" />}
              />
            </div>
          </form>
        </div>
        <Snackbar
          open={(error || errorMessage) ? true : false}
          message={error || errorMessage}
          autoHideDuration={4000}
        />
        <Snackbar
          open={success}
          message='Password Updated Successfully'
          autoHideDuration={4000}
        />


        { this.props.redirectedLogin && this.props.history.push('/') }
      </div>

    );

  }
}

export default withRouter(reduxForm({
  form: 'resetPasswordPage',
})(ResetPasswordPage));