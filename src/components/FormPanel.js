import React from 'react';
import classNames from 'classnames';
import '../static/css/component-form.css';


const FormPanel = (props) => {
  const formClass = classNames ({
    'form-panel' : true,
    'remove-margin' : props.margin,
    'body-text-center': props.center
  });

  let styles = {margin: 'auto'};
  if (props.styles) {
    styles = {...styles, ...props.styles}
  }

  return (
    <div className={formClass} style={styles}>
      {
        props.title && (
          <div className="form-heading">
            <h5>{props.title}</h5>
          </div>
        )
      }
      <div className="form-body">
        {props.children}
      </div>
    </div>
  );
};


export default FormPanel;
