import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';

class RoutesList extends Component {
  constructor (props) {
    super(props);

    this.state = {
      visible: []
    };

    this.handleClick = this.handleClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    let itemArray = Object.values(nextProps.menuList).reduce((data, article) => {
      if (article.submenuID === 0) {
        data.push(false)
      }

      return data
    },[]);
    this.setState({
      visible: itemArray
    })
  }

  handleClick(key) {
    //this.state.visible[key] = !this.state.visible[key];
    this.setState({
      visible: this.state.visible[key]
    });
  }

  render() {
    const {menuList} = this.props;
    let finalMenuList = [];
    let BasicArray = Object.values(menuList);
    let groupedArray = _.groupBy(BasicArray, item => item.menuID);
    for (let [index, value] of Object.entries(groupedArray)) {
      finalMenuList.push({ ID: index, menuID: value[0].menuText, submenuIDs: value.filter(item => item.submenuID !== 0).map(item => item.menuURL), activeFlag: value.filter(item => item.submenuID !== 0).map(item => item.activeFlag), menuText: value.filter(item => item.submenuID !== 0).map(item => item.menuText) })
    }
    return (
      <div className="sidebar-nav">
        <ul>
          <li>
            <Link to="/"><span className="material-icons">home</span>Home</Link>
          </li>
          <li className="divider"></li>
          {
            finalMenuList.map((menu, i) => {
              return (
                <li className="has-child" key={i}>
                  <span className="header" onClick={() => this.handleClick(i)}><Link to="#"><i className="material-icons">create</i>{menu.menuID}</Link>{ this.state.visible[i] === true ? <ContentRemove /> : <ContentAdd/> }</span>
                  <div className="child-item">{ this.state.visible[i] === true ? <ul>{ menu.submenuIDs.map((item, j) => <Link to={ menu.activeFlag[j] === 1 ? `${item}` : '#' } key={j}><ul>{ menu.menuText[j] }</ul></Link>) }</ul> : null }</div>
                </li>
              )
            })
          }
        </ul>
      </div>
    );
  }
}

export default RoutesList;
