import React, { Component } from 'react';
import Panel from './Panel.js';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

class ModulesPage extends Component {
  componentDidMount() {
    let activeValue = Object.values(this.props.menuList && Object.values(this.props.menuList).map(item => Object.values(item).indexOf('modules'))).find(function (item) {
      return item > -1;
    });
    if (!this.props.modules.length && (activeValue !== undefined)) {
      this.props.fetchModules();
    } else if (activeValue === undefined) {
      this.props.history.push('/undefined');
    }
  }

  render() {
    return (
      <Panel title="User Approval">
        <Table>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>#</TableHeaderColumn>
              <TableHeaderColumn>Module ID</TableHeaderColumn>
              <TableHeaderColumn>Module Image</TableHeaderColumn>
              <TableHeaderColumn>Module Desc</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {
              this.props.modules.map((module, index) => {
                return (
                  <TableRow key={module.id}>
                    <TableRowColumn>{index + 1}</TableRowColumn>
                    <TableRowColumn>{module.moduleID}</TableRowColumn>
                    <TableRowColumn>{module.moduleIDimage}</TableRowColumn>
                    <TableRowColumn>{module.moduledesc}</TableRowColumn>
                  </TableRow>
                );
              })
            }
          </TableBody>
        </Table>
      </Panel>
    )
  }
}

export default ModulesPage;
