import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const ApproveSignUp = ({handleApprove}) => (
  <div className="login-wrapper">
    <div className="login-fields text-center">
      <h3><i className="material-icons">done_all</i></h3>
      <div className="pt20">
        <p>Congratulations!! Sign Up process is complete.</p>
        <p>Done</p>
        <RaisedButton label="Done" primary={true} onClick={handleApprove} />
      </div>
    </div>
  </div>
);

export default ApproveSignUp;
