import React, { Component } from 'react';
import { GridList, GridTile } from 'material-ui/GridList';
import { Field, reduxForm } from 'redux-form';
import { RadioButton } from 'material-ui/RadioButton';
import { RadioButtonGroup } from 'redux-form-material-ui';
import RaisedButton from 'material-ui/RaisedButton';
import Drawer from 'material-ui/Drawer';
import Panel from 'components/Panel';
import SchoolNameSignUp from 'containers/signUpFlow/SchoolNameSignUp';

class SignUpSchoolSelect extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      radioButton: {
        display: 'inline-block',
        width: '150px',
      },
      paginationButtons: {
        margin: 5,
        minWidth: 10
      }
    };
    this.schoolTypeOptions = [
      {value: "EL", label: "Elementary"},
      {value: "MD", label: "Middle"},
      {value: "HG", label: "High"}
    ];
  }

  componentDidMount() {
    if (this.props.schools.length === 0) {
      const {stateCD, districtCD} = this.props.profile
      this.props.fetchSchools({stateCD, districtCD});
    }
  }

  getPagination() {
    const { schools, page, handlePageClick } = this.props;
    const totalPages = Math.ceil(schools.length / 16);

    if (totalPages < 2) {
      return null;
    }

    const paginationStart = (page - 3) > 0 ? page - 3 : 1;
    const paginationEnd = (page + 3) <= totalPages ? page + 3 : totalPages;
    const pagination = []
    for (let i = paginationStart; i <= paginationEnd; i++ ) {
      if (page === i) {
        pagination.push(
          <RaisedButton key={i} primary={true} label={i} style={this.styles.paginationButtons} />
        )
      } else {
        pagination.push(
          <RaisedButton key={i} label={i} onClick={() => handlePageClick(i)} style={this.styles.paginationButtons} />
        )
      }
    }
    return pagination;
  }

  render() {
    const {
      schools, page, handleFilterChange, openSignUpForm, schoolForm
    } = this.props;

    const currentPageElements = (page - 1) * 16;
    const schoolsSet = schools.slice(currentPageElements, currentPageElements + 16);

    return (
      <div>
        <Drawer width={'50%'} openSecondary={true} open={schoolForm.open}>
          {
            schoolForm.schoolID ? (
              <SchoolNameSignUp schoolID={ schoolForm.schoolID } />
            ) : null
          }
        </Drawer>
        <h2>Select School</h2>
        <Field name="schoolsFilter" component={RadioButtonGroup} onChange={handleFilterChange}>
          {
            this.schoolTypeOptions.map((option, index) => (
              <RadioButton
                key={index}
                value={option.value}
                label={option.label}
                style={this.styles.radioButton}
              />
            ))
          }
        </Field>
        <Panel>
          <GridList
            cellHeight={180}
            cols={4}
            padding={20}
          >
            {
              schoolsSet.map((school, index) => (
                <GridTile
                  key={index}
                  title={school.Name}
                  rows={1}
                  onClick={() => openSignUpForm(true, school.schoolID)}
                >
                  <img
                    src={`https://loremflickr.com/320/240/school?random=${index}`}
                    alt={school.Name}
                  />
                </GridTile>
              ))
            }
          </GridList>
        </Panel>
        <div className="readmin-footer">
          {
            this.getPagination()
          }
        </div>
      </div>
    )
  }
}

export default reduxForm({
  form: 'signUpScreens',
  enableReinitialize: true,
})(SignUpSchoolSelect);
