import React, { Component } from 'react';
import { Field, reduxForm, FieldArray } from 'redux-form';
import Panel from 'components/Panel';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import { TextField } from 'redux-form-material-ui';
import ActionDelete from 'material-ui/svg-icons/action/delete-forever';
import {fullWhite} from 'material-ui/styles/colors';

const required = value => (value == null ? 'Required' : undefined);
const email = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email'
    : undefined);

const styles = {
  flatButton: {
    marginTop: 25,
  },
};

class AddTeachersForm extends Component {
  constructor (props) {
    super(props);

    this.state = {
      FirstName: '',
      LastName: '',
      UserEmail: '',
      savePreviousData: []
    };
  }

  setFirstName = (e) => {
    this.setState({ FirstName: e.target.value });
  };

  setLastName = (e) => {
    this.setState({ LastName: e.target.value });
  };

  setUserEmail = (e) => {
    this.setState({ UserEmail: e.target.value });
  };

  initiateState = () => {
    this.state.savePreviousData.push({ FirstName: this.state.FirstName, LastName: this.state.LastName, UserEmail: this.state.UserEmail });
    this.setState({
      FirstName: '',
      LastName: '',
      UserEmail: '',
      savePreviousData: this.state.savePreviousData
    })
  };

  rowDelete = (index) => {
    if (this.state.savePreviousData.length === 0) {
      this.setState({ FirstName: '', LastName: '', UserEmail: '' });
    } else if (this.state.savePreviousData.length === 1) {
       let removeArray = this.state.savePreviousData.splice(index-1, 1);
      this.setState({
        FirstName: removeArray[0].FirstName,
        LastName: removeArray[0].LastName,
        UserEmail: removeArray[0].UserEmail
      });
    } else {
      this.setState({
        FirstName: this.state.savePreviousData[this.state.savePreviousData.length - 1].FirstName,
        LastName: this.state.savePreviousData[this.state.savePreviousData.length - 1].LastName,
        UserEmail: this.state.savePreviousData[this.state.savePreviousData.length - 1].UserEmail
      }, function () {
        this.state.savePreviousData.splice(index-1, 1);
        this.setState({ savePreviousData: this.state.savePreviousData })
      });
    }
  };

  render() {

    const {handleSubmit, error, handleFormSubmit, errorMessage, freeze } = this.props;
    const { FirstName, LastName, UserEmail } = this.state;
    return (
      <Panel title="Add Teachers" styles={{width: '60%'}}>
        {error && <strong>{error}</strong>}
        {errorMessage && <strong>{errorMessage}</strong>}
        <form onSubmit={handleSubmit(handleFormSubmit.bind(this))}>
          <FieldArray name="teachers" component={RenderTeachers} props={{setFirstName: this.setFirstName, setLastName: this.setLastName, setUserEmail: this.setUserEmail, values: this.state, initiate: this.initiateState, rowOneDelete: this.rowDelete }} />
          <div style={{ textAlign: 'center' }}>
            <RaisedButton label="Submit" primary={true} type="submit" disabled={freeze || (!FirstName || !LastName || !UserEmail) }/>
          </div>
        </form>
      </Panel>
    )
  }
}

const RenderTeachers = ({ fields, meta: { error, submitFailed }, setFirstName, setLastName, setUserEmail, values, initiate, rowOneDelete }) => {
  const addRow = () => {
    if (fields.length === 0) {
      fields.push({});
    } else {
      fields.push({});
      initiate();
    }
  };

  const rowDelete = (index) => {
    fields.remove(index);
    rowOneDelete(index);
  };

  return (
    <div>
      <div className="row">
        <div className="col-sm-8"></div>
        <div className="col-sm-4">
          <RaisedButton
            label="Add New Teacher"
            backgroundColor="#a4c639"
            labelColor={fullWhite}
            fullWidth={true}
            // onClick={() => fields.push({})}
            onClick={addRow}
            disabled={(!values.FirstName || !values.LastName || !values.UserEmail) && (fields.length !== 0)}
          />
        </div>
      </div>
      {submitFailed && error && <span>{error}</span>}

      {fields.map((member, index) => (
        <div key={index} className="inline">
          <div className="row">
            <div className="col-sm-3">
              <Field
                name={`${member}.FirstName`}
                component={TextField}
                hintText="First Name"
                floatingLabelText="First Name"
                validate={required}
                style = {{width: 150}}
                onChange={setFirstName}
              />
            </div>
            <div className="col-sm-3">
              <Field
                name={`${member}.LastName`}
                component={TextField}
                hintText="Last Name"
                floatingLabelText="Last Name"
                style = {{width: 150}}
                validate={required}
                onChange={setLastName}
              />
            </div>
            <div className="col-sm-3">
              <Field
                name={`${member}.userEMAIL`}
                component={TextField}
                hintText="Email"
                floatingLabelText="Email"
                validate={[required, email]}
                style = {{width: 150}}
                onChange={setUserEmail}
              />
            </div>
            <div className="col-sm-2">
              <FlatButton
                icon={<ActionDelete />}
                className="inline-btn"
                style={styles.flatButton}
                // onClick={() => fields.remove(index)}
                onClick={() => rowDelete(index)}
              />
            </div>
          </div>
        </div>
      ))}
    </div>
  )
};


export default reduxForm({
  form: 'addTeachersForm',
  initialValues: {
    teachers: [{}]
  }
})(AddTeachersForm);


