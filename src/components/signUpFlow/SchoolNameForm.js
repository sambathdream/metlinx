import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Panel from 'components/Panel';
import RaisedButton from 'material-ui/RaisedButton';
import { TextField } from 'redux-form-material-ui';

const required = value => (value == null ? 'Required' : undefined);
const email = value =>
  (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email'
    : undefined);

const phoneNumber = value =>
  (value && !/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(value)
    ? 'Please add a valid Phone number in format (123)-456-7890'
    : undefined);

const styles = {
  raisedButton: {
    margin: 12,
  }
};

class SchoolNameForm extends Component {
  render() {
    const {school, openSignUpForm, handleSubmit, handleFormSubmit, error, errorMessage, freeze} = this.props;
    return (
      <Panel title={school.Name}>
        <p>{school.countyCD}, {school.countyname}, {school.districtName}</p>
        {error && <strong>{error}</strong>}
        {errorMessage && <strong>{errorMessage}</strong>}
        <form onSubmit={handleSubmit(handleFormSubmit.bind(this, school.schoolID, school.schoolCD))}>
          <div>
            <Field
              name="principalEmailAddress"
              component={TextField}
              hintText="Principal Email Address"
              floatingLabelText="Principal Email Address"
              validate={[required, email]}
            />
          </div>
          <div>
            <Field
              name="adminContactEmail"
              component={TextField}
              hintText="Admin Email Address"
              floatingLabelText="Admin Email Address"
              validate={[required, email]}
            />
          </div>
          <div>
            <Field
              name="schoolPhone"
              component={TextField}
              hintText="School Phone Number"
              floatingLabelText="School Phone Number"
              validate={[required, phoneNumber]}
            />
          </div>
          <div>
            <RaisedButton label="Submit" primary={true} style={styles.raisedButton} type="submit" disabled={freeze} />
            <RaisedButton
              style={styles.raisedButton}
              label="Cancel"
              primary={true}
              onClick={() => openSignUpForm(false, '')}
            />
          </div>
        </form>
      </Panel>
    )
  }
}

export default reduxForm({
  form: 'schoolNameForm',
})(SchoolNameForm);
