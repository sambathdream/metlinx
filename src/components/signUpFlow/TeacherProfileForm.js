import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import Panel from 'components/Panel';
import ReactCrop from 'react-image-crop';
import {TextField, Toggle} from 'redux-form-material-ui';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';
import FileInput from 'components/formFields/FileInput';
import 'react-image-crop/dist/ReactCrop.css'

const required = value => (value == null ? 'Required' : undefined);
const email = value =>
    (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Invalid email'
        : undefined);

const styles = {
    paper: {
        height: 150,
        width: 150,
        textAlign: 'center',
    },
};
let schoolLevelCode = localStorage.getItem('schoolLevel'); // assign this to the levelCD code below later
var data = {
    levelCD: schoolLevelCode
};

var subjectArr = [];
var gradeArr = [];

class TeacherProfileForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            gradeChecked: [],
            subjectChecked: [],
            firstName: '',
            lastName: '',
            emailAddress: '',
            subjects: [],
            grades: []
        }
    }

    componentDidMount() {
        this.props.getReadAllSubjects(data);
        this.props.getReadAllGradeLevels(data);
    }

    componentWillUpdate(nextProps, nextState) {
        let gradeIndex = 1;
        let gradeData = nextState.gradeChecked.reduce((result, item, i) => {
            if (item === true) {
                result.push({
                    id: gradeIndex,
                    memberID: 17,
                    gradeCD: i + 1
                });
                gradeIndex++;
            }
            return result
        }, []);
        gradeArr = gradeData;
        let subjectIndex = 1;
        let subjectData = nextState.subjectChecked.reduce((result, item, i) => {
            if (item === true) {
                result.push({
                    id: subjectIndex,
                    memberID: 17,
                    subjectID: i + 1
                });
                subjectIndex++;
            }
            return result
        }, []);
        subjectArr = subjectData
    }

    componentWillReceiveProps(nextProps) {
        nextProps && this.updateGrade(nextProps.gradeLevels)
        nextProps && this.updateSubject(nextProps.subjects)
    }

    updateGrade = (data) => {
        var newGradeCheckArr = [];
        for (let i = 0; i < data.length; i++) {
            newGradeCheckArr.push(false)
        }

        this.setState({gradeChecked: newGradeCheckArr})
    };

    updateSubject = (data) => {
        var newSubjectArr = [];
        for (let i = 0; i < data.length; i++) {
            newSubjectArr.push(false)
        }

        this.setState({subjectChecked: newSubjectArr})
    };

    updateGradeCheck = (num) => {
        var newGradeArr = this.state.gradeChecked;
        newGradeArr[num] = !newGradeArr[num];
        this.setState({gradeChecked: newGradeArr})
    };

    updateSubjectCheck = (num) => {
        var newSubjectsArr = this.state.subjectChecked;
        newSubjectsArr[num] = !newSubjectsArr[num];
        this.setState({subjectChecked: newSubjectsArr})
    };

    onImageLoaded = (image) => {
        const cropHeight = (image.width / image.height) * 20;
        this.props.handleCropChange({width: 20, height: cropHeight});
    };

    render() {
        const {
            handleFormSubmit, profilePicCrop, handleImageUpload,
            profilePic, handleCropChange, crop, file, imageError,
            openCropper, handleCropperClose, handleCropperSubmit,
            freezeCropper, subjects, gradeLevels, handleSubmit
        } = this.props;

        const uploadImage = (
            <div className="col-md-6">
                <Paper style={styles.paper} zDepth={5}>
                    <img style={{width: 'inherit'}} src={`${process.env.REACT_APP_IMAGE_SERVER}${profilePicCrop}`}
                         alt='profile pic'/>
                </Paper>
                {imageError && <p><b>{imageError}</b></p>}
                <RaisedButton
                    containerElement='label'
                    primary={true}
                    label="Upload Image"
                    style={{margin: 10}}
                >
                    <Field
                        component={FileInput}
                        accept=".png,.jpg,.jpeg"
                        onChange={handleImageUpload}
                        name="teacherProfilePic"
                    />
                </RaisedButton>
            </div>
        );

        const imageCropper = (
            <div className="col-md-6">
                <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                    <ReactCrop
                        src={profilePic}
                        onChange={handleCropChange}
                        crop={crop}
                        disabled={freezeCropper}
                        onImageLoaded={this.onImageLoaded}
                    />
                </div>
                <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: 5}}>
                    <FlatButton
                        label="Cancel"
                        primary={true}
                        onClick={handleCropperClose}
                        disabled={freezeCropper}
                    />
                    <FlatButton
                        label="Submit"
                        primary={true}
                        keyboardFocused={true}
                        disabled={freezeCropper}
                        onClick={() => handleCropperSubmit(file, crop)}
                    />
                </div>
            </div>
        );

        return (
            <Panel title="Teacher's Profile" margin={true}>
                <div className="row">
                    <div className="col-md-12">
                        <form onSubmit={handleSubmit(handleFormSubmit.bind(this, subjectArr, gradeArr))}>
                            <div style={{display: 'flex', justifyContent: 'space-between', margin: '0 5%'}}>
                                <div style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    justifyContent: 'flex-start',
                                    alignItems: 'center'
                                }}>
                                    <div>
                                        <Field
                                            name="firstName"
                                            component={TextField}
                                            hintText="First Name"
                                            floatingLabelText="First Name"
                                            validate={required}
                                            onChange={(e) => this.setState({firstName: e.currentTarget.value})}
                                        />
                                    </div>
                                    <div>
                                        <Field
                                            name="lastName"
                                            component={TextField}
                                            hintText="Last Name"
                                            floatingLabelText="Last Name"
                                            validate={required}
                                            onChange={(e) => this.setState({lastName: e.currentTarget.value})}
                                        />
                                    </div>
                                    <div>
                                        <Field
                                            name="email"
                                            type="email"
                                            component={TextField}
                                            hintText="Email"
                                            floatingLabelText="Email"
                                            validate={[required, email]}
                                            onChange={(e) => this.setState({emailAddress: e.currentTarget.value})}
                                        />
                                    </div>
                                    <div style={{marginTop: 15, width: '256px'}}>
                                        <Field
                                            name="pbis"
                                            component={Toggle}
                                            label="PBIS Teacher"
                                            labelPosition="right"
                                        />
                                    </div>
                                </div>
                                <div style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    justifyContent: 'center',
                                    alignItems: 'flex-end'
                                }}>{openCropper ? imageCropper : uploadImage}</div>
                            </div>
                            <div className="checkbox-area" style={{width: '100%', marginTop: '3%'}}>
                                {/* <div className="topic-area"
                                     style={{display: 'flex', justifyContent: 'space-between', fontWeight: '500', margin: '0 auto', width: '90%'}}>
                                    <span>what SUBJECTS does he/she teach?</span>
                                    <span>what GRADES does he/she teach?</span>
                                </div> */}
                                <div className="checkbox" style={{width: '98%', margin: '2% auto', display: 'flex', justifyContent: 'space-between'}}>
                                    <div className="left" style={{
                                        width: '100%',
                                        display: 'flex',
                                        flexDirection: 'column',
                                        marginLeft: '10%',
                                        justifyContent: 'flex-start'
                                    }}>
                                        <span style={{ fontWeight: '500' }}>what SUBJECTS does he/she teach?</span>
                                        <div className="checkbox-content" style={{
                                            width: '80%',
                                            border: '1px solid black',
                                            borderRadius: '5px',
                                            marginTop: '5%'
                                        }}>
                                            <div style={{
                                                width: '100%',
                                                borderBottom: '1px solid black',
                                                paddingLeft: '10px',
                                                backgroundColor: '#268cf2'
                                            }}><span style={{color: 'white'}}>Subjects</span></div>
                                            {
                                                subjects && subjects.map((item, i) => {
                                                    return (
                                                        <Checkbox
                                                            key={i}
                                                            onCheck={() => this.updateSubjectCheck(i)}
                                                            label={item.subjectName}
                                                            labelPosition="left"
                                                            style={{
                                                                borderBottom: '1px solid black',
                                                                paddingLeft: '10px'
                                                            }}
                                                        />
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                    <div className="right" style={{display: 'flex', flexDirection: 'column', width: '100%', justifyContent: 'flex-end'}}>
                                        <span style={{ fontWeight: '500' }}>what GRADES does he/she teach?</span>
                                        <div className="checkbox-content" style={{
                                            width: '80%',
                                            marginRight: '10%',
                                            border: '1px solid black',
                                            borderRadius: '5px',
                                            height: '125px',
                                            overflow: 'scroll',
                                            marginTop: '5%'
                                        }}>
                                            <div style={{
                                                width: '100%',
                                                borderBottom: '1px solid black',
                                                paddingLeft: '10px',
                                                backgroundColor: '#268cf2'
                                            }}><span style={{color: 'white'}}>Grades</span></div>
                                            {
                                                gradeLevels && gradeLevels.map((item, i) => {
                                                    return (
                                                        <Checkbox
                                                            key={i}
                                                            onCheck={() => this.updateGradeCheck(i)}
                                                            label={item.gradeDesc}
                                                            labelPosition="left"
                                                            style={{
                                                                borderBottom: '1px solid black',
                                                                paddingLeft: '10px'
                                                            }}
                                                        />
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="submit-btn" style={{margin: '5% auto', textAlign: 'center'}}>
                                <RaisedButton
                                    label="Save"
                                    primary={true}
                                    type="submit"
                                />
                            </div>
                        </form>
                    </div>
                </div>
            </Panel>
        )
    }
}

export default reduxForm({
    form: 'teacherProfile',
    enableReinitialize: true,
})(TeacherProfileForm);
