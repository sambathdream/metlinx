import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Panel from 'components/Panel';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import { TextField, Toggle } from 'redux-form-material-ui';

const required = value => (value == null ? 'Required' : undefined);
const number = value =>
  (value && !/^\d*$/.test(value)
    ? 'Invalid number'
    : undefined);

class TeacherSignUpForm extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {
    const { teacher, handleFormSubmit } = this.props;
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        onClick={handleFormSubmit}
      />,
    ];
    return (
      <Panel title="Teacher Sign Up" >
        <p>Name: {teacher.firstName} {teacher.lastName}</p>
        <p>Email Address: {teacher.email}</p>
        <form onSubmit={ handleFormSubmit }>
          <div>
            <Field
              name="username"
              component={TextField}
              hintText="Username"
              floatingLabelText="Username"
              validate={required}
            />
          </div>
          <div>
            <Field
              name="password"
              type="password"
              component={TextField}
              floatingLabelText="Password"
              hintText="Password"
              validate={required}
            />
          </div>
          <div>
            <Field
              name="confirmPassword"
              type="password"
              component={TextField}
              floatingLabelText="Confirm Password"
              hintText="Confirm Password"
              validate={required}
            />
          </div>
          <div>
            <Field
              name="phone"
              component={TextField}
              floatingLabelText="Phone"
              hintText="Phone"
              validate={[required, number]}
            />
          </div>
          <div>
            <Field
              name="optForMessage"
              component={Toggle}
              label="Opt For Messages"
              labelPosition="right"
            />
          </div>
          <div>
            <RaisedButton label="Next" primary={true} onClick={this.handleOpen} />
            <Dialog
              title="Enter verification code received on mobile:"
              actions={actions}
              modal={true}
              open={this.state.open}
              autoScrollBodyContent={true}
            >
              <div>
                <p>Only actions can close this dialog.</p>
                <Field
                  name="confirmPhone"
                  type="password"
                  component={TextField}
                  hintText="Verification Code"
                  floatingLabelText="Verification Code"
                  validate={required}
                />
              </div>
            </Dialog>
          </div>
        </form>
      </Panel>
    )
  }
}

export default reduxForm({
  form: 'teacherSignUp',
})(TeacherSignUpForm);
