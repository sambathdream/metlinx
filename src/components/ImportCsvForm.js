import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { RadioButton } from 'material-ui/RadioButton';
import { RadioButtonGroup } from 'redux-form-material-ui';
import Panel from './Panel';
import RaisedButton from 'material-ui/RaisedButton';
import FileInput from './formFields/FileInput';
import Dialog from 'material-ui/Dialog';
import Checkbox from 'material-ui/Checkbox';
import CircularProgress from 'material-ui/CircularProgress';
import FlatButton from 'material-ui/FlatButton';

const csvUploadSteps = [
  'File encryption',
  'File uploaded to secure server',
  'File is being loaded into databases',
];

class ImportCsvForm extends Component {
  componentDidMount () {
    let activeValue = Object.values(this.props.menuList && Object.values(this.props.menuList).map(item => Object.values(item).indexOf('importcsv'))).find(function (item) {
      return item > -1;
    });
    if (activeValue === undefined) {
      this.props.history.push('/undefined');
    }
  }
  render() {
    const {
      handleCsvLevel, handleSubmit, handleFormSubmit,
      error, errorMessage, freeze, csvDialog, encryptCsv,
      closeCsvDialog, success,
    } = this.props;

    const csvSteps = csvUploadSteps.map((step, index) => {
      return (
        <Checkbox
          key={index}
          label={step}
          checked={0 === index || 1 === index ? encryptCsv : success ? true : false}
          uncheckedIcon={<CircularProgress size={20} thickness={3} />}
        />
      )
    });

    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        keyboardFocused={true}
        onClick={closeCsvDialog}
      />,
    ];

    return (
      <Panel title="Import CSV">
        {error && <strong style={{color: 'red'}}>{error}</strong>}
        <form onSubmit={handleSubmit(handleFormSubmit.bind(this))}>
          <div>
            <p><b>Select CSV Type</b></p>
            <Field
              name="csvLevel"
              component={RadioButtonGroup}
              onChange={handleCsvLevel}
            >
              <RadioButton value="school" label="School Level" />
              <RadioButton value="grade" label="Grade Level" />
            </Field>
          </div>
          <div>
            <RaisedButton
              containerElement='label'
              label="Upload CSV"
              style={{margin: '10px 0px 10px 0px'}}
            >
              <Field
                component={FileInput}
                accept=".csv"
                name="csvFile"
              />
            </RaisedButton>
          </div>
          <div>
            <RaisedButton label="Submit" primary={true} type="submit" disabled={freeze} />
          </div>
            <div>
                <RaisedButton label="Close" primary={false} type="submit" disabled={!freeze} />
            </div>
        </form>
        <Dialog
          title="Uploading Students Data"
          modal={true}
          open={csvDialog}
          actions={actions}
        >
          {
            errorMessage.length > 0 ? (
              <div className="alert alert-danger" role="alert">
                <strong style={{color: 'red'}}>{errorMessage}</strong>
              </div>
            ) : csvSteps
          }
        </Dialog>
      </Panel>
    )
  }
}

export default reduxForm({
  form: 'importCsvForm',
  enableReinitialize: true,
})(ImportCsvForm)
