import React, { Component } from 'react';
import Panel from './Panel.js';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import {fullWhite} from 'material-ui/styles/colors';

class UserRequest extends Component {
  componentDidMount() {
    if (!this.props.requestedUsers.length) {
      this.props.fetchRequestedUsers();
    }
  }

  showTableData () {
    return this.props.requestedUsers.map((user) => {
      return (
        <TableRow key={user.id}>
          <TableRowColumn>{user.FirstName}</TableRowColumn>
          <TableRowColumn>{user.LastName}</TableRowColumn>
          <TableRowColumn>{user.userEMAIL}</TableRowColumn>
          <TableRowColumn>
            <RaisedButton
              backgroundColor="#a4c639"
              label="Approve"
              labelColor={fullWhite}
              onClick={() => this.props.handleUserApproval(user.id)}
            />
          </TableRowColumn>
        </TableRow>
      );
    });
  }

  render() {
    return (
      <Panel title="User Approval">
        <Table>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>First Name</TableHeaderColumn>
              <TableHeaderColumn>Last Name</TableHeaderColumn>
              <TableHeaderColumn>Email ID</TableHeaderColumn>
              <TableHeaderColumn>Action</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
              {this.showTableData()}
          </TableBody>
        </Table>
      </Panel>
    )
  }
}

export default UserRequest;
