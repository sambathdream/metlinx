import React from 'react';
import classNames from 'classnames';

const Panel = (props) => {
  const panelClass = classNames ({
    'readmin-panel' : true,
    'remove-margin' : props.margin,
    'body-text-center': props.center
  });

  let styles = {margin: 'auto'};
  if (props.styles) {
    styles = {...styles, ...props.styles}
  }

  return (
    <div className={panelClass} style={styles}>
      {
        props.title && (
          <div className="panel-heading">
            <h5>{props.title}</h5>
          </div>
        )
      }
      <div className="panel-body">
        {props.children}
      </div>
    </div>
  );
}


export default Panel;
