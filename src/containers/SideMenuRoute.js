import { connect } from 'react-redux';
import SideMenuRoute from '../components/SideMenuRoute';

const mapStateToProps = state => {
    return {
        loggedIn: state.login.success,
        flowStatus: state.profile.flowStatus,
    };
};

export default connect(
    mapStateToProps,
)(SideMenuRoute);
