import {connect} from 'react-redux';
import AvatarDropdown from 'components/AvatarDropdown';
import {userLogout} from '../store/actions/userauth';

const mapStateToProps = state => {
    return {

        loggedIn: state.login.success,
        flowStatus: state.profile.flowStatus,
        logoutMessage: state.menuList.logoutMessage
    };
};

const mapDispatchToProps = dispatch => {
    return {
        handleLogout: () => {
            dispatch(userLogout());
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(AvatarDropdown);
