import { connect } from 'react-redux';
import ImportCsvForm from '../components/ImportCsvForm';
import {csvLevel, saveCsvData, closeCsvDialog} from '../store/actions/importCsv';
import { SubmissionError } from 'redux-form'

const csvTypes = [
  'text/plain',
  'text/x-csv',
  'application/vnd.ms-excel',
  'application/csv',
  'application/x-csv',
  'text/csv',
  'text/comma-separated-values',
  'text/x-comma-separated-values',
  'text/tab-separated-values',
];

const mapStateToProps = state => {
  return {
    initialValues: {csvLevel: state.importCsv.csvLevel},
    freeze: state.importCsv.freeze,
    errorMessage: state.importCsv.error,
    encryptCsv: state.importCsv.encryptCsv,
    csvDialog: state.importCsv.csvDialog,
    success: state.importCsv.success,
    menuList: state.menuList.menuList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handleCsvLevel: (event, value) => {
      dispatch(csvLevel(value));
    },
    handleFormSubmit: (values) => {
      if (!values.csvLevel) {
        throw new SubmissionError({
          _error: 'Error: Please Select CSV Type !!'
        })
      }
      if (!values.csvFile || csvTypes.indexOf(values.csvFile.type) === -1) {
        throw new SubmissionError({
          _error: 'Please upload a valid CSV file'
        })
      }
      dispatch(saveCsvData(values));
    },
    closeCsvDialog: () => {
      dispatch(closeCsvDialog());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ImportCsvForm);
