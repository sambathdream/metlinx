import { connect } from 'react-redux';
import ResetPasswordPage from '../components/ResetPasswordPage';
import { SubmissionError } from 'redux-form';
import { resetPassword } from '../store/actions/resetPassword';

let storedUser = null;

const mapStateToProps = state => {
  return {
    freeze: state.resetPassword.freeze,
    errorMessage: state.resetPassword.error,
    success: state.resetPassword.success,
    redirectedLogin: state.resetPassword.redirectedLogin
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handleFormSubmit: (values) => {
      if (values.password !== values.confirmPassword) {
        throw new SubmissionError({
          confirmPassword: 'Password doesn\'t match',
          _error: 'Change Password Failed!'
        })
      }
      storedUser = localStorage.getItem('userEMAIL');
      dispatch(resetPassword({userEMAIL: storedUser, password: values.password}));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResetPasswordPage);
