
import {connect} from 'react-redux';
import SchoolRegistrationForm from '../components/schoolRegistration/schoolRegistrationForm';
import {fetchStates, fetchCountiesAndDistricts,submitSchoolInfo} from '../store/actions/schoolRegistration';




const mapStateToProps = state => {
    return {
        states: state.schoolRegistration.states,
        districts: state.schoolRegistration.districts,
        counties: state.schoolRegistration.counties,
        freeze: state.schoolRegistration.freeze,
        errorMessage: state.schoolRegistration.error,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        handleFormSubmit: (values) => {
            dispatch(submitSchoolInfo(values));
        },
        handleStateChange: (event, value) => {
            dispatch(fetchCountiesAndDistricts(value))
        },
        getAllStates: () => {
            dispatch(fetchStates());
        },

    }
};


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SchoolRegistrationForm)