import { connect } from 'react-redux';
import StudentsPage from '../../components/students/StudentsList';
import {
    handleImageChange,
    cropChange,
    uploadStudentImage,
    errorImageSelection,
    toggleCropper,
    getStudentList,
    toggleStudentForm,
    handleCloseForm,
    createStudentForm,
    openStudentForm
} from '../../store/actions/students'

const mapStateToProps = state => {
    return {
        studentsPicCrop: state.students.studentsPicCrop,
        studentsPic: state.students.studentsPic,
        crop: state.students.crop,
        file: state.students.file,
        imageError: state.students.imageError,
        openCropper: state.students.openCropper,
        freezeCropper: state.students.freezeCropper,
        studentLists: state.students.studentLists,
        formOpen: state.students.form.open,
        initialForm: state.students.initialForm
    };
};

const mapDispatchToProps = dispatch => {
    return {
        handleImageUpload: (event, file) => {
            if (!/image\/(jpe?g|png|gif|bmp)$/i.test(file.type)) {
                dispatch(errorImageSelection('Please select Image of type PNG, JPEG, JPG.'));
            } else {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onloadend = () => {
                    dispatch(handleImageChange(file, reader.result));
                }
            }
        },
        handleCropChange: (crop) => {
            dispatch(cropChange(crop));
        },
        handleCropperClose: () => {
            dispatch(toggleCropper());
        },
        handleCropperSubmit: (file, crop) => {
            dispatch(uploadStudentImage(file, crop));
        },
        getStutdentList: () => {
            dispatch(getStudentList());
        },
        toggleStudentForm: (id, open) => {
            dispatch(toggleStudentForm(id, open));
        },
        handleCloseForm: () => {
            dispatch(handleCloseForm())
        },
        openStudentForm: (open) => {
            dispatch(openStudentForm(open));
        },
        handleFormSubmit: (sendInvite, dateOfBirth, values) => {
            var formData = {
                    student: {
                        studentID: values.studentID,
                        FirstName: values.FirstName,
                        lastName: values.lastName,
                        gradeCD: values.gradeCD,
                        gender: values.gender,
                        dateOfBirth: dateOfBirth,
                        schoolCardID: values.schoolCardID,
                        mobilePhone: values.mobilePhone,
                        profilePictureLoc: values.profilePictureLoc,
                        schoolID: values.schoolID,
                        studentEMAIL: "teststudent2@mail.com",
                        notes: values.notes,
                        addressID: values.addressID
                    },
                    parent: {
                        FirstName: values.parentFirstName,
                        LastName: values.parentLastName,
                        userEMAIL: "parenttest2@mail.com",
                        mobilePhone: values.parentMobilePhone,
                        sendinvite: sendInvite
                    }
                }
            
            dispatch(createStudentForm(formData))
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(StudentsPage);