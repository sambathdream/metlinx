import {connect} from 'react-redux';
import AdminPage from '../components/Admin';

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {}
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(AdminPage);