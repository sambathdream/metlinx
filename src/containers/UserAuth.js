import { connect } from 'react-redux';
import UserAuthForm from '../components/UserAuth/UserAuthForm';
import { submitLogin } from '../store/actions/userauth';


const mapStateToProps = state => {
    return {
        freeze: state.login.freeze,
        errorMessage: state.login.error,
        success: state.login.success,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        handleFormSubmit: values => {
            dispatch(submitLogin(values))
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(UserAuthForm);
