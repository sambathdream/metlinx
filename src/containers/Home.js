import { connect } from 'react-redux';
import Home from '../components/Home';
import {fetchMenuList} from '../store/actions/sidebar';

const mapStateToProps = state => {
  return {
    menuList: state.menuList.menuList,
  };
};

const mapDispatchToProps = dispatch => {
  return {

  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
