import { connect } from 'react-redux';
import FeatureForm from '../../components/features/FeatureForm';
import { toggleFeatureForm, createFeatureRequest } from '../../store/actions/features';
import { fetchModules } from '../../store/actions/modules';
import { SubmissionError } from 'redux-form';

const getFeature = (features, id) => {
  return features.find(feature => {
    return feature.id === id;
  });
}

const mapStateToProps = state => {
  return {
    modules: state.modules.modules,
    initialValues: getFeature(state.features.features, state.features.form.id),
    id: state.features.form.id,
    errorMessage: state.features.error,
    freeze: state.features.freeze,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleFeatureForm: (id, open) => {
      dispatch(toggleFeatureForm(id, open));
    },
    fetchModules: () => {
      dispatch(fetchModules());
    },
    handleFormSubmit: (values) => {
      if (!values.featureName) {
        throw new SubmissionError({
          featureName: 'Feature name is required',
          _error: 'Feature creation failed'
        });
      } else if (!values.featureDesc) {
        throw new SubmissionError({
          featureDesc: 'Feature description is required',
          _error: 'Feature creation failed'
        });
      } else if (!values.featureURL) {
        throw new SubmissionError({
          featureURL: 'URL is required',
          _error: 'Feature creation failed'
        });
      }
      dispatch(createFeatureRequest(values));
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FeatureForm);
