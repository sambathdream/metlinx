import { connect } from 'react-redux';
import FeaturesList from '../../components/features/FeaturesList';
import { fetchModules } from '../../store/actions/modules';
import {
  fetchFeatures,
  deleteFeatureRequest,
  toggleFeatureForm,
  featureFilterChange,
  filterModuleIDChange,
} from '../../store/actions/features';

function getVisibleFeatures(state) {
  const {filter: {enabled, moduleID}, features} = state.features;
  if (enabled) {
    return features.filter(feature => {
      return feature.moduleID === moduleID;
    });
  }
  return features;
}

function mapStateToProps(state) {
  return {
    features: getVisibleFeatures(state),
    form: state.features.form,
    filter: state.features.filter,
    modules: state.modules.modules,
    menuList: state.menuList.menuList,
  };
}

const mapDispatchToProps = dispatch => {
  return {
    fetchFeatures: () => {
      dispatch(fetchFeatures());
    },
    fetchModules: () => {
      dispatch(fetchModules());
    },
    onFeatureDelete: id => {
      dispatch(deleteFeatureRequest(id));
    },
    toggleFeatureForm: (id, open) => {
      dispatch(toggleFeatureForm(id, open));
    },
    onFeatureFilterChange: (value) => {
      dispatch(featureFilterChange(value));
    },
    onFilterModuleIDChange: (event, value) => {
      dispatch(filterModuleIDChange(value));
    },
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FeaturesList);
