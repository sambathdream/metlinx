import { connect } from 'react-redux';
import PrivateRoute from '../components/PrivateRoute';

const mapStateToProps = state => {
  return {
    loggedIn: state.login.success,
    flowStatus: state.profile.flowStatus,
  };
};

export default connect(
  mapStateToProps,
)(PrivateRoute);
