import { connect } from 'react-redux';
import {fetchRequestedUsers, userApproved} from '../store/actions/requestedUsers';
import UserRequest from '../components/UserRequest';

const mapStateToProps = state => {
  return {
    requestedUsers: state.requestedUsers.users,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchRequestedUsers: () => {
      dispatch(fetchRequestedUsers());
    },
    handleUserApproval: (userId) => {
      dispatch(userApproved(userId));
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserRequest);
