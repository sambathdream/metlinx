/**
 * Created by ggm on 3/26/18.
 */
import {connect} from 'react-redux';
import ParentRegistrationForm from '../../components/ParentAndStudentRegistration/ParentRegistrationForm';

const mapStateToProps = state => {
    return {
        // initialValues: state.parentAndStudentRegistration.initialForm && state.parentAndStudentRegistration.initialForm.parent
    };
};

const mapDispatchToProps = dispatch => {
    return {}
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ParentRegistrationForm)