/**
 * Created by ggm on 3/26/18.
 */
import {connect} from 'react-redux';
import StudentRegistrationForm from '../../components/ParentAndStudentRegistration/StudentRegistrationForm';

const mapStateToProps = state => {
    return {
        // initialValues: state.parentAndStudentRegistration.initialForm && state.parentAndStudentRegistration.initialForm.student[0]
    };
};

const mapDispatchToProps = dispatch => {
    return {}
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StudentRegistrationForm)