/**
 * Created by ggm on 3/26/18.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
    Step,
    Stepper,
    StepLabel,
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

import ParentRegistration from './ParentRegistration';
import StudentRegistration from './StudentRegistration';

import {
    getParentProfile,
    registerParentAndStudentProfile,
    saveFormData
} from '../../store/actions/parentandstudentregistration';

const stepperStyle = {
    display: 'flex',
    flexDirection: 'row',
    placeContent: 'center space-between',
    alignItems: 'center',
    width: '40%',
    margin: '0 auto'
};

class ParentAndStudentRegistration extends Component {
    constructor(props) {
        super(props);

        this.state = {
            finished: false,
            stepIndex: 0,
        }
    }

    componentDidMount() {
        // this.props.getParentProfile()
    }

    handleNext = () => {
        this.props.saveFormData(this.state.stepIndex, this.props.formState);
        const {stepIndex} = this.state;
        this.setState({stepIndex: stepIndex + 1}, function () {
            if(stepIndex >= 1) {
                this.setState({finished: true}, function () {
                    const formData = {
                        parent: this.props.parentFormData,
                        student: this.props.studentFormData
                    };
                    this.props.registerParentProfile(formData);
                    this.props.history.push('/');
                })
            } else {
                this.setState({finished: false})
            }
        });
    };

    handlePrev = () => {
        const {stepIndex} = this.state;
        if (stepIndex > 0) {
            this.setState({stepIndex: stepIndex - 1});
        }
    };

    getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return <ParentRegistration/>;
            case 1:
                return <StudentRegistration/>;
            default:
                return (
                    <div>Something went wrong!!</div>
                )
        }
    }

    render() {
        const {finished, stepIndex} = this.state;
        const contentStyle = {margin: '0 16px'};

        return (
            <div style={{width: '100%', margin: 'auto'}}>
                <Stepper activeStep={stepIndex} style={stepperStyle}>
                    <Step>
                        <StepLabel>Parent Profile</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Student Information</StepLabel>
                    </Step>
                </Stepper>
                <div style={contentStyle}>
                    {finished ? (
                        <p>
                            <a
                                //href="#"
                                onClick={(event) => {
                                    event.preventDefault();
                                    this.setState({stepIndex: 0, finished: false});
                                }}
                            >
                                Click here
                            </a> to reset the example.
                        </p>
                    ) : (
                        <div>
                            <div>{this.getStepContent(stepIndex)}</div>
                            <div style={{marginTop: 12, display: 'flex', justifyContent: 'space-around'}}>
                                <FlatButton
                                    label="Previous"
                                    disabled={stepIndex === 0}
                                    onClick={this.handlePrev}
                                    style={{marginRight: 12}}
                                />
                                <RaisedButton
                                    label={stepIndex === 1 ? 'Finish' : 'Next'}
                                    primary={true}
                                    onClick={this.handleNext}
                                />
                            </div>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        formState: state.form,
        parentFormData: state.parentAndStudentRegistration.parentFormData,
        studentFormData: state.parentAndStudentRegistration.studentFormData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getParentProfile: () => {
            dispatch(getParentProfile())
        },
        registerParentProfile: (data) => {
            dispatch(registerParentAndStudentProfile(data))
        },
        saveFormData: (step, data) => {
            dispatch(saveFormData(step, data))
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ParentAndStudentRegistration)