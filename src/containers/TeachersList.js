import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form'
import TeachersListForm from '../components/TeachersListForm';
import {createTeachersList, closeTLDialog} from '../store/actions/TeachersList';

const mapStateToProps = state => {
    return {
        errorMessage: state.addTeachers.error,
        freeze: state.TeachersList.freeze,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        handleFormSubmit: (values) => {
            if (!values.teachers.length) {
                throw new SubmissionError({
                    _error: 'Error: Please add Teachers !!'
                })
            }
            dispatch(createTeachersList(values))
        },

        closeTLDialog: () => {
                dispatch(closeTLDialog())
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,

)(TeachersListForm);
