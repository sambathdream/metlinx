import { connect } from 'react-redux';
import RolesPage from '../components/RolesPage';
import {fetchRoles} from '../store/actions/roles';

const mapStateToProps = state => {
  return {
    roles: state.roles.roles,
    menuList: state.menuList.menuList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchRoles: () => {
      dispatch(fetchRoles());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RolesPage);
