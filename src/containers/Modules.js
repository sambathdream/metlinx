import { connect } from 'react-redux';
import ModulesPage from '../components/ModulesPage';
import {fetchModules} from '../store/actions/modules';

const mapStateToProps = state => {
  return {
    modules: state.modules.modules,
    menuList: state.menuList.menuList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchModules: () => {
      dispatch(fetchModules());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ModulesPage);
