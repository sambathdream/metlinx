import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Drawer from 'material-ui/Drawer';
import {toggleSidebar, fetchMenuList} from '../store/actions/sidebar';
import RoutesList from '../components/RoutesList';
import { Link } from 'react-router-dom';

class Sidebar extends Component {
  // menu collapse when on mobile function
  menuCollapseWithResize = () => {
    if (window.innerWidth < 991) {
      this.props.toggleSidebar(false);
    }
    if (window.innerWidth > 991) {
      this.props.toggleSidebar(true);
    }
  }

  // Sidebar collapse when tablet
  componentDidMount () {
    window.addEventListener('resize', this.menuCollapseWithResize);

    if (window.innerWidth < 991) {
      this.props.toggleSidebar(false);
    }

    if (Object.keys(this.props.menuList).length === 0) {
      this.props.fetchMenuList();
    }
  }

  // Sidebar collapse when tablet
  componentWillUnmount() {
    window.removeEventListener('resize', this.menuCollapseWithResize);
  }

  // Sidebar toggle
  toggleMenu = () => {
    this.props.toggleSidebar(!this.props.sidebarOpen);
  }

  render() {
    // Sidebar class based on bg color
    const sidebarClass = classNames ({
      'menu-drawer' : true,
      'has-bg': true,
    });

    return(
      <div className="readmin-sidebar">
        <div className="an-header">
          <div className="header-left">
            <Link className="brand" to="/">Menu</Link>
            <FloatingActionButton
              mini={true}
              secondary={true}
              onClick={this.toggleMenu}
            >
            <i className="material-icons">format_indent_decrease</i>
            </FloatingActionButton>
          </div>
        </div>
        <Drawer open={this.props.sidebarOpen}
          className={sidebarClass}
          containerClassName="sidebar-initial-color"
          containerStyle={{ background: '#202432' }}
        >
          <Scrollbars>
            <RoutesList menuList={this.props.menuList}/>
          </Scrollbars>
        </Drawer>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    sidebarOpen: state.sidebar.menu,
    menuList: state.menuList.menuList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleSidebar: open => {
      dispatch(toggleSidebar(open));
    },
    fetchMenuList: () => {
      dispatch(fetchMenuList());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Sidebar);
