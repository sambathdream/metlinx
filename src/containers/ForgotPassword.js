import { connect } from 'react-redux';
import ForgotPasswordPage from '../components/ForgotPasswordPage';
import {forgotPassword} from '../store/actions/forgotPassword';

const mapStateToProps = state => {
  return {
    freeze: state.forgotPassword.freeze,
    errorMessage: state.forgotPassword.error,
    success: state.forgotPassword.success,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handleFormSubmit: (values) => {
      dispatch(forgotPassword(values));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgotPasswordPage);
