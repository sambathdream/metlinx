import { connect } from 'react-redux';
import ProfileForm from 'components/profiles/ProfileForm';
import { fetchModules } from '../../store/actions/modules';
import { fetchRoles } from '../../store/actions/roles';
import { fetchFeatures } from '../../store/actions/features';
import { SubmissionError } from 'redux-form';
import { toggleProfileForm, createProfile } from '../../store/actions/profiles';

const featureSelected = (modules) => {
  for (let key in modules) {
    if (modules[key].length > 0) {
      return true;
    }
  }
  return false;
}

const mapStateToProps = state => {
  return {
    roles: state.roles.roles,
    features: state.features.features,
    profiles: state.profiles.profiles,
    errorMessage: state.profiles.error,
    freeze: state.profiles.freeze,
    initialValues: state.profiles.form.profileID ? state.profiles.profiles[state.profiles.form.profileID] : {},
    profileID: state.profiles.form.profileID,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleProfileForm: (id, open) => {
      dispatch(toggleProfileForm(id, open));
    },
    fetchRoles: () => {
      dispatch(fetchRoles());
    },
    fetchModules: () => {
      dispatch(fetchModules());
    },
    fetchFeatures: () => {
      dispatch(fetchFeatures());
    },
    handleFormSubmit: (values) => {
      if (!values.roles || values.roles.length === 0) {
        throw new SubmissionError({
          roles: 'Choose atleast one Role',
          _error: 'Profile creation failed'
        });
      } else if (!values.profileName) {
        throw new SubmissionError({
          profileName: 'Profile name is required',
          _error: 'Profile creation failed'
        });
      } else if (!values.profileDesc) {
        throw new SubmissionError({
          profileDesc: 'Profile description is required',
          _error: 'Profile creation failed'
        });
      } else if (!values.modules || !featureSelected(values.modules)) {
        throw new SubmissionError({
          modules: 'Profile description is required',
          _error: 'Select atleast one Module Feature',
        });
      }
      dispatch(createProfile(values));
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileForm);
