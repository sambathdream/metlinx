import { connect } from 'react-redux';
import ProfilesList from '../../components/profiles/ProfilesList';
import {fetchProfiles, toggleProfileForm, deleteProfile, onInputChange, profileFilterChange} from '../../store/actions/profiles';

function getVisibleProfiles(state) {
  const {filter: { enabled, filterText }, profiles} = state;
  if (enabled && filterText.length > 2) {
    const filteredProfiles = {};
    const expression = new RegExp(filterText, 'gi');
    Object.keys(profiles).forEach(key => {
      if (  profiles[key]['profileName'].search(expression) > -1
        ||  profiles[key]['profileDesc'].search(expression) > -1
      ) {
        return filteredProfiles[key] = profiles[key];
      }

      const textInRoles = profiles[key]['roles'].find(role => {
        return role.search(expression) > -1;
      });

      if (textInRoles) {
        return filteredProfiles[key] = profiles[key];
      }

      if (profiles[key].modules) {
        const { modules } = profiles[key];
        const textInModules = Object.keys(modules).find(module => {
          if (module.search(expression) > -1) {
            return true;
          }
          return false;
        });

        if (textInModules) {
          return filteredProfiles[key] = profiles[key];
        }
      }
    });
    return filteredProfiles;
  }
  return profiles;
}

function mapStateToProps(state) {
  return {
    profiles: getVisibleProfiles(state.profiles),
    filter: state.profiles.filter,
    features: state.features.features,
    form: state.profiles.form,
    menuList: state.menuList.menuList
  };
}

const mapDispatchToProps = dispatch => {
  return {
    fetchProfiles: () => {
      dispatch(fetchProfiles());
    },
    toggleProfileForm: (id, open) => {
      dispatch(toggleProfileForm(id, open));
    },
    onProfileDelete: (profileID) => {
      dispatch(deleteProfile(profileID));
    },
    onInputChange: (event, value) => {
      event.preventDefault();
      dispatch(onInputChange(value));
    },
    onProfileFilterChange: (value) => {
      dispatch(profileFilterChange(value));
    },
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilesList);
