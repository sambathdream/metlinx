import { connect } from 'react-redux';
import SchoolNameForm from 'components/signUpFlow/SchoolNameForm';
import { openSignUpForm } from '../../store/actions/schools';
import { submitSchoolName } from '../../store/actions/schoolName';

function getSchool(schools, schoolID) {
  return schools.find(school => {
    return school.schoolID === schoolID;
  })
}

const mapStateToProps = (state, ownProps) => {
  return {
    school: getSchool(state.schools.schools, ownProps.schoolID),
    freeze: state.schoolName.freeze,
    errorMessage: state.schoolName.error,
  };
}

const mapDispatchToProps = dispatch => {
  return {
    openSignUpForm: (open, schoolID) => {
      dispatch(openSignUpForm(open, schoolID));
    },
    handleFormSubmit: (schoolID, schoolCD, values) => {
      dispatch(submitSchoolName({...values, schoolID, schoolCD}));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SchoolNameForm);
