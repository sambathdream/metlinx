import { connect } from 'react-redux';
import TeacherSignUpForm from '../../components/signUpFlow/TeacherSignUpForm';

const mapStateToProps = state => {
  return {
    teacher: state.teacherSignUp.teacher
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handleFormSubmit: (values) => {
      debugger;
    },
    handlePhoneVerification: (values) => {
      debugger;
    }
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TeacherSignUpForm);
