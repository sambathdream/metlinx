import {connect} from 'react-redux';
import TeacherProfileForm from '../../components/signUpFlow/TeacherProfileForm';
import {
    fetchSubjects,
    handleImageChange,
    cropChange,
    uploadProfileImage,
    errorImageSelection,
    toggleCropper,
    getReadAllSubjects,
    getReadAllGradeLevels,
    updateProfile
} from '../../store/actions/teacherProfile';

const mapStateToProps = state => {
    return {
        profilePicCrop: state.teacherProfile.profilePicCrop,
        profilePic: state.teacherProfile.profilePic,
        crop: state.teacherProfile.crop,
        file: state.teacherProfile.file,
        imageError: state.teacherProfile.imageError,
        openCropper: state.teacherProfile.openCropper,
        freezeCropper: state.teacherProfile.freezeCropper,
        subjects: state.teacherProfile.subjects,
        gradeLevels: state.teacherProfile.gradeLevels
    };
};

const mapDispatchToProps = dispatch => {
    return {
        handleFormSubmit: (subjectArr, gradeArr, values) => {
            var formData = {
                memberRec: {
                    memberID: 17,
                    userID: values.email,
                    schoolID: 510162000681,
                    profileID: 6,
                    FirstName: values.firstName,
                    LastName: values.lastName,
                    gender: null,
                    dateOfBirth: null,
                    schoolCardID: null,
                    profilePictureLoc: null,
                    workPhone: null,
                    extension: null,
                    mobilePhone: null,
                    skypeID: null,
                    twitterID: null,
                    facebookID: null,
                    instagramID: null,
                    linkedID: null,
                    userEMAIL: values.email,
                    notes: null,
                    addressID: null,
                    standardFlag: 0
                },
                memberSubjectsRec: subjectArr,
                memberGradesRec: gradeArr
            };
            dispatch(updateProfile(formData));
        },
        fetchSubjects: () => {
            dispatch(fetchSubjects());
        },
        handleImageUpload: (event, file) => {
            if (!/image\/(jpe?g|png|gif|bmp)$/i.test(file.type)) {
                dispatch(errorImageSelection('Please select Image of type PNG, JPEG, JPG.'));
            } else {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onloadend = () => {
                    dispatch(handleImageChange(file, reader.result));
                }
            }
        },
        handleCropChange: (crop) => {
            dispatch(cropChange(crop));
        },
        handleCropperClose: () => {
            dispatch(toggleCropper());
        },
        handleCropperSubmit: (file, crop) => {
            dispatch(uploadProfileImage(file, crop));
        },
        getReadAllSubjects: (data) => {
            dispatch(getReadAllSubjects(data));
        },
        getReadAllGradeLevels: (data) => {
            dispatch(getReadAllGradeLevels(data))
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TeacherProfileForm);
