import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form'
import AddTeachersForm from '../../components/signUpFlow/AddTeachersForm';
import {createTeachersProfiles} from '../../store/actions/addTeachers';

const mapStateToProps = state => {
    return {
        errorMessage: state.addTeachers.error,
        freeze: state.addTeachers.freeze,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        handleFormSubmit: (values) => {
            if (!values.teachers.length) {
                throw new SubmissionError({
                    _error: 'Error: Please add Teachers !!'
                })
            }
            dispatch(createTeachersProfiles(values));
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(AddTeachersForm);
