import React, { Component } from 'react';
import { connect } from 'react-redux';
import NotificationWrapper from '../../components/NotificationWrapper';
import TeacherSignUp from './TeacherSignUp';
import SignUpSchool from './SignUpSchool';
import AddTeachers from './AddTeachers';
import ApproveSignUpCompletion from './ApproveSignUpCompletion';
import { Redirect } from 'react-router-dom';
import {
  Step,
  Stepper,
  StepLabel,
} from 'material-ui/Stepper';

class SignUpFlow extends Component {
  getStepContent(stepIndex) {
    switch(this.props.flowStatus) {
      case 100:
        return (
          <NotificationWrapper
            materialIcon={<i className="material-icons">email</i>}
            notification="Please click on the link sent on your email. Thanks!"
          />
        );
      case 300:
        return (
          <NotificationWrapper
            materialIcon={<i className="material-icons">pan_tool</i>}
            notification="Awaiting sales team approval. Thanks!"
          />
        );
      case 400:
        return (
          <SignUpSchool />
        );
      case 500:
        return <AddTeachers />;
      case 600:
        return <ApproveSignUpCompletion />;
      case 'teacherSignUp':
        return <TeacherSignUp />;
      default:
        return (
          <div>Something went wrong!!</div>
        )
    }
  }

  render() {
    return (
      <div style={{width: '100%', margin: 'auto'}}>
        <Stepper activeStep={Math.floor((this.props.flowStatus - 1) / 100)}>
          <Step>
            <StepLabel>Approve Email</StepLabel>
          </Step>
          <Step>
            <StepLabel>School Location</StepLabel>
          </Step>
          <Step>
            <StepLabel>Sales Team Approval</StepLabel>
          </Step>
          <Step>
            <StepLabel>Select School</StepLabel>
          </Step>
          <Step>
            <StepLabel>Add Teachers</StepLabel>
          </Step>
          <Step>
            <StepLabel>Done</StepLabel>
          </Step>
        </Stepper>
        <div style={{margin: '0 16px'}}>
          {
            800 === this.props.flowStatus ? (
              <Redirect to={{ pathname: '/' }} />
            ) : this.getStepContent(this.props.flowStatus)
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    flowStatus: state.profile.flowStatus,
  };
};

export default connect(
  mapStateToProps,
)(SignUpFlow);
