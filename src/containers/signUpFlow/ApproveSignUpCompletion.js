import { connect } from 'react-redux';
import ApproveSignUp from '../../components/signUpFlow/ApproveSignUp';
import {signUpApprove} from '../../store/actions/schoolRegistration';

const mapDispatchToProps = dispatch => {
  return {
    handleApprove: () => {
      dispatch(signUpApprove());
    },
  }
};

export default connect(
  null,
  mapDispatchToProps,
)(ApproveSignUp);
