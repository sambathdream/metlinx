import { connect } from 'react-redux';
import SignUpSchoolSelect from '../../components/signUpFlow/SignUpSchoolSelect';
import { schoolPageChange, schoolFilterChange, openSignUpForm, fetchSchools } from '../../store/actions/schools';

function getFilteredSchools(schools, filter) {
  return schools.filter(school => {
    return filter === school.levelCD
  })
}

const mapStateToProps = state => {
  return {
    page: state.schools.page,
    schools: getFilteredSchools(state.schools.schools, state.schools.filter),
    profile: state.profile,
    initialValues: {schoolsFilter: state.schools.filter},
    schoolForm: state.schools.form,
  };
};

const mapDispatchToProps = (dispatch, state) => {
  return {
    handlePageClick: (page) => {
      dispatch(schoolPageChange(page));
    },
    handleFilterChange: (e) => {
      dispatch(schoolFilterChange(e.target.value));
    },
    openSignUpForm: (open, schoolID) => {
      dispatch(openSignUpForm(open, schoolID));
    },
    fetchSchools: (values) => {
      dispatch(fetchSchools(values));
    },
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignUpSchoolSelect);
