import { connect } from 'react-redux';
import React, { Component } from 'react';
import LinearProgress from 'material-ui/LinearProgress';

class SignUpProgress extends Component {
  render() {
    return(
      <LinearProgress mode="determinate" value={this.props.flowStatus / 7} />
    );
  }
}

const mapStateToProps = state => {
  return {
    flowStatus: state.profile.flowStatus,
  };
};

export default connect(
  mapStateToProps,
)(SignUpProgress);
