import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import {BrowserRouter as Router} from 'react-router-dom'
import classNames from 'classnames';
import {connect} from 'react-redux';

import SignUpFlow from 'containers/signUpFlow';
import SchoolRegistration from 'containers/schoolRegistration'
import ParentAndStudentRegistration from 'containers/ParentAndstudentRegistration';
import UserAuth from 'containers/UserAuth';
import PrivateRoute from 'containers/PrivateRoute';
import UserApproval from 'containers/UserApproval';
import Admin from 'containers/Admin';
import Home from 'containers/Home';
import Header from 'containers/Header';
import Footer from 'components/Footer';
import Sidebar from 'containers/Sidebar';
import Roles from 'containers/Roles';
import Modules from 'containers/Modules';
import Features from 'containers/features/Features';
import Profiles from 'containers/profiles/Profiles';
import StudentInformation from 'containers/students/Students';
import TeacherProfile from 'containers/signUpFlow/TeacherProfile';
import ForgotPassword from 'containers/ForgotPassword';
import ResetPassword from 'containers/ResetPassword';
import ImportCsv from 'containers/ImportCsv';
import TeacherList from 'containers/TeachersList';
import SideMenuRoute from 'containers/SideMenuRoute';


class RoutesComponent extends Component {


    render() {
        // Page content class change based on menu toggle
        const pageContent = classNames({
            'readmin-page-content': true,
            'menu-open': this.props.flowStatus === 800 && this.props.sidebarOpen
        });

        return (
            <Router>
                <div>
                    {
                        this.props.flowStatus >= 800 && <Sidebar/>

                    }

                    <div className={pageContent}>
                        <Header/>
                        <Switch>

                            <PrivateRoute exact path="/" component={Home}/>
                            <Route exact path="/admin" component={Admin}/>
                            <Route exact path="/login" component={UserAuth}/>
                            <Route exact path="/userapproval" component={UserApproval}/>
                            <Route path="/signup" component={SchoolRegistration}/>
                            <Route path="/forgotpassword" component={ForgotPassword}/>
                            <Route path="/resetPW" component={ResetPassword}/>
                            <PrivateRoute path="/signupflow" component={SignUpFlow}/>
                            <PrivateRoute path="/parentandstudentregistration"
                                          component={ParentAndStudentRegistration}/>
                            <PrivateRoute path="/roles" component={Roles}/>
                            <PrivateRoute path="/modules" component={Modules}/>
                            <PrivateRoute path="/features" component={Features}/>
                            <PrivateRoute path="/profiles" component={Profiles}/>
                            <PrivateRoute path="/students" component={StudentInformation}/>
                            <PrivateRoute path="/teacherprofile" component={TeacherProfile}/>
                            <PrivateRoute path="/importcsv" component={ImportCsv}/>
                            <SideMenuRoute path="/teacherlist" component={TeacherList}/>
                        </Switch>
                        <Footer/>
                    </div>
                </div>
            </Router>
        );
    }
}

const mapStateToProps = state => {
    return {
        sidebarOpen: state.sidebar.menu,
        flowStatus: state.profile.flowStatus,
    };
};

export default connect(
    mapStateToProps,
)(RoutesComponent);
