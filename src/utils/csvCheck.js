import {getCsvStruct} from '../store/api/api';

export function checkStudentsCsv(file, tableName) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsText(file);
    reader.onload = async function(e) {
      const csvFields = await getCsvStruct(tableName);
      const allTextLines = reader.result.split(/\r\n|\n/);
      const csvFileHeaders = allTextLines[0].split(',');
      const isAllHeaderPresent = csvFields.find(csvField => {
        if (csvField.Column_Name === 'id') {
          return false;
        }
        if (csvFileHeaders.indexOf(csvField.Column_Name) > -1) {
          return false;
        }
        return true;
      });
      if (isAllHeaderPresent) {
        reject('Please refer sample CSV file, Uploaded file missing required headers');
      }
      allTextLines.splice(0, 1);
      const emptyElements = allTextLines.find(line => {
        let data = line.split(',');
        if (data.includes('')) {
          return true;
        }
        return false;
      });
      if (emptyElements) {
        reject('File should not contain any empty colums');
      } else {
        resolve();
      }
    }
  });
}
