import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';

//+ ----------------
//
//
//
//
//
//
//
//+ ----------------


import {getUserProfile} from './store/api/api';

function render(initialState) {
    const store = configureStore(initialState);

    ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
        document.getElementById('root')
    );
}

getUserProfile()
    .then(data => {
        if (data.error) {
            console.log(data.error);
            render();
        } else {
            render({
                'profile': data[0],
                'login': {
                    success: true,
                }
            });
        }
    })
    .catch((err) => {
        console.log(err);
        render();
    });


