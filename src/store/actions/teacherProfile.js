import {
    FETCH_SUBJECTS,
    IMAGE_CHANGE,
    TOGGLE_CROPPER,
    CROP_CHANGE,
    UPLOAD_PROFILE_PIC,
    TEACHER_PROFILE_IMAGE_ERROR,
    GET_READ_SUBJECTS,
    GET_READ_GRADELEVEL,
    UPDATE_TEACHER_PROFILE
} from '../actionTypes/index';

export function fetchSubjects() {
    return {
        type: FETCH_SUBJECTS,
    };
}

export function handleImageChange(file, profilePic) {
    return {
        type: IMAGE_CHANGE,
        file,
        profilePic,
    }
}

export function toggleCropper() {
    return {
        type: TOGGLE_CROPPER,
    }
}

export function cropChange(crop) {
    return {
        type: CROP_CHANGE,
        crop,
    }
}

export function uploadProfileImage(file, crop) {
    return {
        type: UPLOAD_PROFILE_PIC,
        file,
        crop,
    }
}

export function errorImageSelection(imageError) {
    return {
        type: TEACHER_PROFILE_IMAGE_ERROR,
        imageError,
    }
}

export function getReadAllSubjects(data) {
    return {
        type: GET_READ_SUBJECTS,
        data
    }
}

export function getReadAllGradeLevels(data) {
    return {
        type: GET_READ_GRADELEVEL,
        data
    }
}

export function updateProfile(data) {
    return {
        type: UPDATE_TEACHER_PROFILE,
        data
    }
}
