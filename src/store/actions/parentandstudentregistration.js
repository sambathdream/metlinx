import {
    GET_PARENT_STUDENT_FORM_INITIAL,
    REGISTER_PARENT_AND_STUDENT_PROFILE,
    SAVE_FORM_DATA
} from '../actionTypes/index';

export function getParentProfile() {
    return {
        type: GET_PARENT_STUDENT_FORM_INITIAL
    };
}

export function registerParentAndStudentProfile(data) {
    return {
        type: REGISTER_PARENT_AND_STUDENT_PROFILE,
        data
    }
}

export function saveFormData(step, data) {

    return {
        type: SAVE_FORM_DATA,
        step,
        data
    }
}