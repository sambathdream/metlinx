import {
  SUBMIT_SCHOOL_NAME,
} from '../actionTypes/index';

export function submitSchoolName(values) {
  return {
    type: SUBMIT_SCHOOL_NAME,
    values
  };
}
