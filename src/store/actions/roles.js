import {
  FETCH_ROLES,
} from '../actionTypes/index';

export function fetchRoles() {
  return {
    type: FETCH_ROLES,
  };
}
