import {
    FETCH_PROFILES,
    PROFILE_FORM,
    CREATE_PROFILE,
    DELETE_PROFILE,
    PROFILE_FILTER,
    PROFILE_FILTER_TEXT,
} from '../actionTypes/index';

export function fetchProfiles() {
    return {
        type: FETCH_PROFILES,
    };
}

export function toggleProfileForm(profileID, open) {
    return {
        type: PROFILE_FORM,
        open,
        profileID,
    };
}

export function createProfile(values) {
    return {
        type: CREATE_PROFILE,
        values,
    };
}

export function deleteProfile(profileID) {
    return {
        type: DELETE_PROFILE,
        profileID,
    };
}

export function profileFilterChange(enabled) {
    return {
        type: PROFILE_FILTER,
        enabled,
    };
}

export function onInputChange(text) {
    return {
        type: PROFILE_FILTER_TEXT,
        text,
    };
}
