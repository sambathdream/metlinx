import {
    SUBMIT_LOGIN,
    USER_LOGOUT,
} from '../actionTypes/index';

export function submitLogin(values) {
    return {
        type: SUBMIT_LOGIN,
        values
    };
}

export function userLogout() {
    return {
        type: USER_LOGOUT,
    };
}
