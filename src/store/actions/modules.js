import {
  FETCH_MODULES,
} from '../actionTypes/index';

export function fetchModules() {
  return {
    type: FETCH_MODULES,
  };
}
