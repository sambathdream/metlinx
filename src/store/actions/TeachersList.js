import {
    CREATE_TEACHERS_LIST,
    CLOSE_TL_DIALOG
} from '../actionTypes/index';

export function createTeachersList(values) {
    return {
        type: CREATE_TEACHERS_LIST,
        values,
    };
}

export function closeTLDialog() {
    return {
        type: CLOSE_TL_DIALOG,
    };
}
