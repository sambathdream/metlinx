import {
  FORGOT_PASSWORD,
} from '../actionTypes/index';

export function forgotPassword(values) {
  return {
    type: FORGOT_PASSWORD,
    values,
  };
}
