import {
    ROLE_CLICKED,
    TEACHER_ROLE_CLICKED,
    PARENT_ROLE_CLICKED,
    STUDENT_ROLE_CLICKED,
    FETCH_STATES,
    FETCH_COUNTIES_DISTRICTS,
    SUBMIT_SCHOOL_INFO,
    SUBMIT_SIGN_UP,
    SIGN_UP_APPROVE
} from '../actionTypes/index';

export function submitSignUp(values) {
    return {
        type: SUBMIT_SIGN_UP,
        values,
    };
}

export function signUpApprove() {
    return {
        type: SIGN_UP_APPROVE,
    };
}

export function fetchStates() {

  return {
    type: FETCH_STATES,
  };
}

export function fetchCountiesAndDistricts(state) {
  return {
    type: FETCH_COUNTIES_DISTRICTS,
    state
  }
}

export function submitSchoolInfo(values) {
  return {
    type: SUBMIT_SCHOOL_INFO,
    values
  }
}
