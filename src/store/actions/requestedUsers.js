import {
  FETCH_REQUESTED_USERS,
  USER_APPROVED,
} from '../actionTypes/index';

export function fetchRequestedUsers() {
  return {
    type: FETCH_REQUESTED_USERS,
  };
}

export function userApproved(userID) {
  return {
    type: USER_APPROVED,
    userID,
  };
}
