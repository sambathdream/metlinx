import {
  RESET_PASSWORD,
} from '../actionTypes/index';

export function resetPassword(values) {
  return {
    type: RESET_PASSWORD,
    values,
  };
}
