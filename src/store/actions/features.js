import {
  FETCH_FEATURES,
  DELETE_FEATURE,
  FEATURE_FORM,
  CREATE_FEATURE,
  FEATURE_FILTER,
  FILTER_MODULEID,
} from '../actionTypes/index';

export function fetchFeatures() {
  return {
    type: FETCH_FEATURES,
  };
}

export function deleteFeatureRequest(id) {
  return {
    type: DELETE_FEATURE,
    id,
  }
}

export function toggleFeatureForm(id, open) {
  return {
    type: FEATURE_FORM,
    open,
    id,
  };
}

export function createFeatureRequest(values) {
  return {
    type: CREATE_FEATURE,
    values,
  };
}

export function featureFilterChange(enabled) {
  return {
    type: FEATURE_FILTER,
    enabled,
  };
}

export function filterModuleIDChange(moduleID) {
  return {
    type: FILTER_MODULEID,
    moduleID,
  };
}
