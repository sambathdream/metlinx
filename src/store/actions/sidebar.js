import {
  SIDEBAR,
  FETCH_MENU_LIST,
} from '../actionTypes/index';

export function toggleSidebar(open) {
  return {
    type: SIDEBAR,
    open,
  };
}

export function fetchMenuList() {
  return {
    type: FETCH_MENU_LIST,
  };
}
