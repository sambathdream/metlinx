import {
    STUDENTS_IMAGE_ERROR,
    STUDENTS_PIC,
    STUDENTS_CROP_CHANGE,
    STUDENTS_TOGGLE_CROPPER,
    STUDENTS_IMAGE_CHANGE,
    GET_STUDENT_LIST,
    STUDENT_FORM,
    HANDLE_CLOSE_FORM,
    CREATE_STUDENT_FORM,
    OPEN_STUDENT_FORM
} from '../actionTypes/index'

export function handleImageChange(file, studentPic) {
    return {
        type: STUDENTS_IMAGE_CHANGE,
        file,
        studentPic,
    }
}

export function toggleCropper() {
    return {
        type: STUDENTS_TOGGLE_CROPPER,
    }
}

export function cropChange(crop) {
    return {
        type: STUDENTS_CROP_CHANGE,
        crop,
    }
}

export function uploadStudentImage(file, crop) {
    return {
        type: STUDENTS_PIC,
        file,
        crop,
    }
}

export function errorImageSelection(imageError) {
    return {
        type: STUDENTS_IMAGE_ERROR,
        imageError,
    }
}

export function getStudentList() {
    return {
        type: GET_STUDENT_LIST
    }
}

export function toggleStudentForm(studentID, open) {
    return {
        type: STUDENT_FORM,
        open,
        studentID,
    };
}

export function openStudentForm(open) {
    return {
        type: OPEN_STUDENT_FORM,
        open
    }
}

export function handleCloseForm() {
    return {
        type: HANDLE_CLOSE_FORM
    }
}

export function createStudentForm(data) {
    return {
        type: CREATE_STUDENT_FORM,
        data
    }
}