import {
  CSV_LEVEL,
  SAVE_CSV,
  CLOSE_CSV_DIALOG,
} from '../actionTypes/index';

export function csvLevel(value) {
  return {
    type: CSV_LEVEL,
    value,
  };
}

export function saveCsvData(values) {
  return {
    type: SAVE_CSV,
    values,
  };
}

export function closeCsvDialog() {
  return {
    type: CLOSE_CSV_DIALOG,
  };
}
