import {
  SCHOOLS_FILTER,
  PAGE_CHANGE,
  FORM_OPEN,
  FETCH_SCHOOLS,
} from '../actionTypes/index';

export function schoolFilterChange(filter) {
  return {
    type: SCHOOLS_FILTER,
    filter,
  };
}

export function schoolPageChange(page) {
  return {
    type: PAGE_CHANGE,
    page,
  };
}

export function openSignUpForm(open, schoolID) {
  return {
    type: FORM_OPEN,
    open,
    schoolID,
  }
}

export function fetchSchools(values) {
  return {
    type: FETCH_SCHOOLS,
    values,
  }
}
