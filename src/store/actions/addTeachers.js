import {
  CREATE_TEACHERS_PROFILES,
} from '../actionTypes/index';

export function createTeachersProfiles(values) {
  return {
    type: CREATE_TEACHERS_PROFILES,
    values,
  };
}
