// this api.js contains only the common functions .. if you are looking for
// any individual functions like userAuth etc.. please look into individual Sagas.





// common getflowstatus.

export const getFlowStatus = () => {
    const email = localStorage.getItem('email');
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!email || !mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/getFlowStatus`,
        {
            method: 'POST',
            body: JSON.stringify({
                userEMAIL: email,
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        .then(json => {
            if (json.error) {
                throw new Error('Authorization Failed');
            }
            return {
                flowStatus: json.flowstatus,
                email,
            };
        })
        ;
};

export const getUserProfile = () => {
    const email = localStorage.getItem('email');
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!email || !mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/getOnboardSignupRec`,
        {
            method: 'POST',
            body: JSON.stringify({
                userEMAIL: email,
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error(response.statusText);
            }
            return response.json();
        })
        ;
};







export const getRoles = () => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/roles`,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const postSchoolInfo = (values) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/postDistrictInfo`,
        {
            method: 'POST',
            body: JSON.stringify({
                ...values,
                "userEMAIL": localStorage.email,
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
};

export const postSchoolName = (values) => {
    const userEMAIL = localStorage.getItem('email');
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!userEMAIL || !mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/updateOnboardWithSchoolinfo`,
        {
            method: 'POST',
            body: JSON.stringify({
                ...values,
                userEMAIL,
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
};

export const getRequestedUsers = () => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/userapproval`,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            return response.json();
        })
        ;
}

export const userApproved = (userID) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/userapproval`,
        {
            method: 'POST',
            body: JSON.stringify({userID}),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            return response.json();
        })
        ;
}

export const createTeachersProfiles = (values) => {
    const userEMAIL = localStorage.getItem('email');
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!userEMAIL || !mxauthorization) {
        return Promise.reject('No localStorage exists');
    }
    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/onboardingteacher`,
        {
            method: 'POST',
            body: JSON.stringify({
                ...values,
                userEMAIL
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}


export const createTeachersList = (values) => {
    const userEMAIL = localStorage.getItem('email');
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!userEMAIL || !mxauthorization) {
        return Promise.reject('No localStorage exists');
    }
    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/onboardingteacher`,
        // Raj:Jul 25th, 2018
        // onboardingteacher is during onboarding the school, what about the maintenance for teachers
         // we need to create a seperate API to fetch teacher list
        //  API should use the schoolID, and bring all the profiles with Role as teacher
        //  this API should be for teacher Maintenance Module from the side menu for Admin
        {
            method: 'POST',
            body: JSON.stringify({
                ...values,
                userEMAIL
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}
export const getModules = () => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/getmodules`,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const getFeatures = () => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/features`,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const removeFeature = (id) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/features/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const createFeatureRequest = (values) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    let requestUrl = `${process.env.REACT_APP_BACKEND}/api/features`;
    let method = 'POST';
    if (values.id) {
        requestUrl = requestUrl + `/${values.id}`;
        method = 'PUT';
    }
    return fetch(
        requestUrl,
        {
            method,
            body: JSON.stringify(values),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const getProfiles = () => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/profiles`,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const createProfileRequest = (values) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    let requestUrl = `${process.env.REACT_APP_BACKEND}/api/profiles`;
    let requestMethod = 'POST';

    if (values.profileID) {
        requestUrl = requestUrl + `/${values.profileID}`;
        requestMethod = 'PUT';
    }

    return fetch(
        requestUrl,
        {
            method: requestMethod,
            body: JSON.stringify(values),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const deleteProfileRequest = (profileID) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/profiles/${profileID}`,
        {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const getSubjects = () => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }
    return Promise.resolve()
    // return fetch(
    //   `${process.env.REACT_APP_BACKEND}/api//${profileID}`,
    //   {
    //     method: 'DELETE',
    //     headers: {
    //       'Accept': 'application/json',
    //       'Content-Type': 'application/json',
    //       mxauthorization,
    //     },
    //   }
    // )
    //   .then(response => {
    //     return response.json();
    //   })
    // ;
}

export const getReadAllSubjects = (content) => {

};

export const approveSignUp = () => {
    const userEMAIL = localStorage.getItem('email');
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!userEMAIL || !mxauthorization) {
        return Promise.reject('No localStorage exists');
    }
    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/approvesignup`,
        {
            method: 'POST',
            body: JSON.stringify({
                userEMAIL
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
        }
    )
        .then(response => {
            return response.json();
        })
        ;
}

export const uploadAndCropImage = (content) => {
    const userEMAIL = localStorage.getItem('email');
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!userEMAIL || !mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    const data = new FormData()
    data.append('file', content.file);
    data.append('userEMAIL', userEMAIL);
    data.append('crop', JSON.stringify(content.crop));

    return fetch(
        `${process.env.REACT_APP_IMAGE_SERVER}/api/teacherprofileimage`,
        {
            method: 'POST',
            body: data,
            headers: {
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const requestForgotPassword = (values) => {
    return fetch(
        `${process.env.REACT_APP_MXAUTH}/api/forgotmypassword`,
        {
            method: 'POST',
            body: JSON.stringify(values),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }
    )
        .then(response => {
            return response.json();
        })
        ;
}

export const requestResetPassword = (values) => {
    const query = window.location.search.substring(1);
    const vars = query.split('&');
    const queryParams = {};
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split('=');
        queryParams[pair[0]] = pair[1];
    }

    if (undefined === queryParams['token']) {
        return Promise.resolve({error: 'You are not Authorized to perform this action.'});
    }

    return fetch(
        `${process.env.REACT_APP_MXAUTH}/api/Updatepwd`,
        {
            method: 'POST',
            body: JSON.stringify(values),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization: queryParams['token'],
            },
        }
    )
        .then(response => {
            return response.json();
        })
        ;
}

export const saveCsvRequest = (content) => {
    const userEMAIL = localStorage.getItem('email');
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!userEMAIL || !mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    const data = new FormData()
    data.append('file', content.csvFile);
    data.append('csvLevel', content.csvLevel);
    if (content.grade) {
        data.append('grade', content.grade);
    }

    return fetch(
        // `${process.env.REACT_APP_IMAGE_SERVER}/api/teacherprofileimage`,
        `${process.env.REACT_APP_BACKEND}/api/csvupload`,
        {
            method: 'POST',
            body: data,
            headers: {
                mxauthorization,
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const getMenuList = () => {
    const userEMAIL = localStorage.getItem('email');
    const data = {"userEMAIL": `${userEMAIL}`}
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/usermenulist`,
        {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            }
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const getCsvStruct = (tableName) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/csvstruct`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
            body: JSON.stringify({tableName}),
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const getAllSubjects = (value) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/readAllSubjects`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
            body: JSON.stringify(value),
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
};

export const getAllGrades = (values) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/readAllGradelevels`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
            body: JSON.stringify(values),
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
};

export const updateTeacherprofile = (values) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/updateTeacherProfile`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
            body: JSON.stringify(values),
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
};

export const getAllStudent = () => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    const email = localStorage.getItem('email');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/getMyStudentList`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
            body: JSON.stringify({userEMAIL: email}),
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
};

export const getInitialForm = (values) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/getstudentinfo`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
            body: JSON.stringify(values),
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
};

export const updateStudentFormData = (values) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/registerParent`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
            body: JSON.stringify(values),
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
};

export const getInitialValuesForParentAndStudentForm = () => {

    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/getParentProfile`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
            body: JSON.stringify({
                userEMAIL: "parenttest2@metlinx.com"
            }),
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
};

export const registerParentAndStudentFormContent = (values) => {
    const mxauthorization = localStorage.getItem('mxauthorization');
    if (!mxauthorization) {
        return Promise.reject('No localStorage exists');
    }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/saveParentProfile`,
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                mxauthorization,
            },
            body: JSON.stringify(values)
            ,
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
};



