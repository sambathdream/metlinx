import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';
import rootSagas from './sagas'
import allReducers from '../store/reducers';

export default function configureStore(preloadedState) {
  const sagaMiddleware = createSagaMiddleware();
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


  //  -- IMPORTANT --: REMOVE REDUX DEVTOOLS FOR FINAL PROD DEPLOYMENT.
    // -- ALSO REMOVE ALL CONSOLELOG STATEMENTS




  const store = createStore(
    allReducers,
    preloadedState,
    composeEnhancers(applyMiddleware(sagaMiddleware, thunkMiddleware )));

  sagaMiddleware.run(rootSagas);

  return store;
}
