import {put, call} from 'redux-saga/effects';
import {delay} from 'redux-saga';

import {
    FREEZE_SIGN_UP,
    SUCCESS_SIGN_UP,
    ERROR_SIGN_UP,
    RESET_STORE,
    RECEIVE_SCHOOLS,
    COUNTIES_DISTRICT_LIST,
    STATES_LIST
} from '../actionTypes/index';

export const getStates = () => {
    console.log('This is from getStates API call');
    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/Allstates`,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'

            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        .then(json => {
            if (json.error) {
                throw new Error('Authorization Failed');
            }
            return json;
        })
        ;
};
export const getDistricts = (stateCD) => {
    // const mxauthorization = localStorage.getItem('mxauthorization');
    // if (!mxauthorization) {
    //     return Promise.reject('No localStorage exists');
    // }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/getDistrictListforState`,
        {
            method: 'POST',
            body: JSON.stringify({
                stateCD
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'

            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}

export const getDistrictSchools = (values) => {
    // const mxauthorization = localStorage.getItem('mxauthorization');
    // if (!mxauthorization) {
    //     return Promise.reject('No localStorage exists');
    // }
    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/getDistrictSchools`,
        {
            method: 'POST',
            body: JSON.stringify(values),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',

            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
};


export const getCounties = (stateCD) => {
    // const mxauthorization = localStorage.getItem('mxauthorization');
    // if (!mxauthorization) {
    //     return Promise.reject('No localStorage exists');
    // }

    return fetch(
        `${process.env.REACT_APP_BACKEND}/api/getCountyListforState`,
        {
            method: 'POST',
            body: JSON.stringify({
                stateCD
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',

            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        ;
}
export const apiToRegisterSchool = (values) => {
    return fetch(
        `${process.env.REACT_APP_ONBOARD}/api/newschoolsignup`,
        {
            method: 'POST',
            body: JSON.stringify({
                userEMAIL: values.email,
                FirstName: values.firstName,
                LastName: values.lastName,
                password: values.password,
                countyCD: values.countyCD,
                districtCD: values.districtCD,
                stateCD: values.stateCD


            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }
    )
        .then(response => {
            return response.json();
        })
        ;
};

export function* schoolsSaga({values}) {
    try {
        const schools = yield call(getDistrictSchools, values);
        yield put({type: RECEIVE_SCHOOLS, schools});
    } catch (error) {
        yield put({type: RESET_STORE});
    }
}

export function* signUpSaga({values}) {
    yield put({type: FREEZE_SIGN_UP});
    try {
        const {error} = yield call(apiToRegisterSchool, values);
        if (error) {
            yield put({type: ERROR_SIGN_UP, error});
        } else {
            yield put({type: SUCCESS_SIGN_UP});
        }
    } catch (error) {
        yield put({type: ERROR_SIGN_UP, error: 'Server Error'});
    }
}
export function* fetchStatesSaga() {
    console.log('%% inside fetchStatesSaga');
    try {
        const states = yield call(getStates);
        yield put({type: STATES_LIST, states});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}

export function* fetchCountiesAndDistricts({state}) {
    try {
        const districts = yield call(getDistricts, state);
        const counties = yield call(getCounties, state);
        yield put({type: COUNTIES_DISTRICT_LIST, districts, counties});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}
