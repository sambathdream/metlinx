import {put, call} from 'redux-saga/effects';
import {delay} from 'redux-saga';

import {
    FREEZE_LOGIN,
    SUCCESS_LOGIN,
    ERROR_LOGIN,
    USER_PROFILE,
    LOG_OUT_MESSAGE,
    RESET_STORE
} from '../actionTypes/index';

import {
    getUserProfile
} from '../api/api'

export const loginRequest = (values) => {
    console.log('%%-%%: this from inside loginRequest API');
    return fetch(
        `${process.env.REACT_APP_ONBOARD}/api/signin`,
        {
            method: 'POST',
            body: JSON.stringify({
                userEMAIL: values.username,
                password: values.password,
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }
    )
        .then(response => {
            if ('Unauthorized' === response.statusText) {
                throw new Error('Unauthorized');
            }
            return response.json();
        })
        .then(json => {
            if (json.token) {
                localStorage.setItem('mxauthorization', json.token);
                localStorage.setItem('email', values.username);
                localStorage.setItem('schoolLevel',json.schoolLevel);
                localStorage.setItem('userROLE', json.userROLE);
                localStorage.setItem('schoolID', json.schoolID);
                localStorage.setItem('userEMAIL',json.username);
                localStorage.setItem('flowStatus',json.flowStatus);
            }
            return json;
        })
        ;
};

export const logoutRequest = () => {
    localStorage.clear();
    return Promise.resolve('Successfull logout');
};

export function* loginSaga({values}) {
    console.log('%%-%% : Inside LoginSaga --');
    yield put({type: FREEZE_LOGIN});
    try {
        const {error: loginError} = yield call(loginRequest, values);
        if (loginError) {
            yield put({type: ERROR_LOGIN, error: loginError});
        } else {
            yield put({type: SUCCESS_LOGIN});
            const {error, ...response} = yield call(getUserProfile);
            yield put({type: USER_PROFILE, data: response[0]});
        }
    } catch (error) {
        yield put({type: ERROR_LOGIN, error: error.message});
    }
}

export function* logoutSaga() {
    yield put({type: LOG_OUT_MESSAGE});
    yield call(delay, 2000);
    yield call(logoutRequest);
    yield put({type: RESET_STORE});
}


