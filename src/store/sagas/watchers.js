import {takeLatest, takeEvery} from 'redux-saga/effects';
import {
    FETCH_STATES,
    FETCH_COUNTIES_DISTRICTS,
    SUBMIT_SCHOOL_INFO,
    FETCH_SCHOOLS,
    SUBMIT_SIGN_UP,
    SUBMIT_LOGIN,
    USER_LOGOUT,
    SUBMIT_SCHOOL_NAME,
    FETCH_REQUESTED_USERS,
    USER_APPROVED,
    CREATE_TEACHERS_PROFILES,
    CREATE_TEACHERS_LIST,
    FETCH_ROLES,
    FETCH_MODULES,
    FETCH_FEATURES,
    DELETE_FEATURE,
    CREATE_FEATURE,
    FETCH_PROFILES,
    CREATE_PROFILE,
    DELETE_PROFILE,
    FETCH_SUBJECTS,
    SIGN_UP_APPROVE,
    UPLOAD_PROFILE_PIC,
    FORGOT_PASSWORD,
    RESET_PASSWORD,
    SAVE_CSV,
    FETCH_MENU_LIST,
    GET_READ_SUBJECTS,
    GET_READ_GRADELEVEL,
    UPDATE_TEACHER_PROFILE,
    STUDENTS_PIC,
    GET_STUDENT_LIST,
    STUDENT_FORM,
    CREATE_STUDENT_FORM,
    GET_PARENT_STUDENT_FORM_INITIAL,
    REGISTER_PARENT_AND_STUDENT_PROFILE
} from '../actionTypes/index';

import {loginSaga, logoutSaga} from './userAuth';
import {
    schoolsSaga,
    signUpSaga,
    fetchStatesSaga,
    fetchCountiesAndDistricts
} from './schoolRegistration'


import {
    submitSchoolInfo,
    submitSchoolName,
    fetchRequestedUsers,
    submitUserApproved,
    addTeachers,
    fetchRoles,
    fetchModules,
    fetchFeatures,
    deleteFeature,
    createFeature,
    fetchProfiles,
    createProfile,
    deleteProfile,
    fetchSubjects,
    signUpApprove,
    uploadProfilePic,
    uploadStudentsPic,
    forgotPassword,
    resetPassword,
    saveCsvData,
    fetchMenuList,
    getReadAllSubjects,
    getReadAllGradeLevels,
    updateTeacherProfile,
    getAllStudentList,
    getStudentForm,
    updateStudentForm,
    getParentAndStudentRegistrationForm,
    registerParentStudentProfile,
    TeachersList
} from './sagas';

export function* watchFetchSchools() {
    yield takeLatest(FETCH_SCHOOLS, schoolsSaga);
}

export function* watchSubmitSignUp() {
    yield takeLatest(SUBMIT_SIGN_UP, signUpSaga);
}

export function* watchSubmitLogin() {
    console.log('%%-%%: Inside WatcherSubmitLogin ');
    yield takeLatest(SUBMIT_LOGIN, loginSaga);
}

export function* watchUserLogout() {
    yield takeLatest(USER_LOGOUT, logoutSaga);
}

export function* watchFetchStates() {
    console.log('inside watcher watchFetchStates');
    yield takeLatest(FETCH_STATES, fetchStatesSaga);
}

export function* watchFetchCountiesDistricts() {
    yield takeLatest(FETCH_COUNTIES_DISTRICTS, fetchCountiesAndDistricts);
}

export function* watchSubmitSchoolInfo() {
    yield takeLatest(SUBMIT_SCHOOL_INFO, submitSchoolInfo);
}

export function* watchSubmitSchoolName() {
    yield takeLatest(SUBMIT_SCHOOL_NAME, submitSchoolName);
}

export function* watchFetchRequestedUsers() {
    yield takeLatest(FETCH_REQUESTED_USERS, fetchRequestedUsers);
}

export function* watchUserApproved() {
    yield takeLatest(USER_APPROVED, submitUserApproved);
}

export function* watchAddTeachers() {
    yield takeLatest(CREATE_TEACHERS_PROFILES, addTeachers);
}

export function* watchTeachersList() {
    yield takeLatest(CREATE_TEACHERS_LIST, TeachersList);
}

export function* watchFetchRoles() {
    yield takeLatest(FETCH_ROLES, fetchRoles);
}

export function* watchFetchModules() {
    yield takeLatest(FETCH_MODULES, fetchModules);
}

export function* watchFetchFeatures() {
    yield takeLatest(FETCH_FEATURES, fetchFeatures);
}

export function* watchDeleteFeature() {
    yield takeEvery(DELETE_FEATURE, deleteFeature);
}

export function* watchCreateFeature() {
    yield takeLatest(CREATE_FEATURE, createFeature);
}

export function* watchFetchProfiles() {
    yield takeLatest(FETCH_PROFILES, fetchProfiles);
}

export function* watchCreateProfile() {
    yield takeLatest(CREATE_PROFILE, createProfile);
}

export function* watchDeleteProfile() {
    yield takeEvery(DELETE_PROFILE, deleteProfile);
}

export function* watchFetchSubjects() {
    yield takeLatest(FETCH_SUBJECTS, fetchSubjects);
}

export function* watchApproveSignUp() {
    yield takeLatest(SIGN_UP_APPROVE, signUpApprove);
}

export function* watchProfileImageUpload() {
    yield takeLatest(UPLOAD_PROFILE_PIC, uploadProfilePic);
}

export function* watchStudentsImageUpload() {
    yield takeLatest(STUDENTS_PIC, uploadStudentsPic)
}

export function* watchForgotPassword() {
    yield takeLatest(FORGOT_PASSWORD, forgotPassword);
}

export function* watchResetPassword() {
    yield takeLatest(RESET_PASSWORD, resetPassword);
}

export function* watchSaveCsv() {
    yield takeLatest(SAVE_CSV, saveCsvData);
}

export function* watchFetchMenuList() {
    yield takeLatest(FETCH_MENU_LIST, fetchMenuList);
}

export function* watchGetReadAllSubjects() {
    yield takeLatest(GET_READ_SUBJECTS, getReadAllSubjects)
}

export function* watchGetReadAllGradeLevels() {
    yield takeLatest(GET_READ_GRADELEVEL, getReadAllGradeLevels)
}

export function* watchUpdateTeacherProfile() {
    yield takeLatest(UPDATE_TEACHER_PROFILE, updateTeacherProfile)
}

export function* watchGetStudentList() {
    yield takeLatest(GET_STUDENT_LIST, getAllStudentList)
}

export function* watchGetStudentForm() {
    yield takeLatest(STUDENT_FORM, getStudentForm)
}

export function* watchUpdateStudentForm() {
    yield takeLatest(CREATE_STUDENT_FORM, updateStudentForm)
}

export function* watchGetParentAndStudentRegistrationFormInitialValues() {
    yield takeLatest(GET_PARENT_STUDENT_FORM_INITIAL, getParentAndStudentRegistrationForm)
}

export function* watchRegisterParentAndStudentProfile() {
    yield takeLatest(REGISTER_PARENT_AND_STUDENT_PROFILE, registerParentStudentProfile)
}

