import {put, call} from 'redux-saga/effects';
import {delay} from 'redux-saga';
import {checkStudentsCsv} from '../../utils/csvCheck';
import {
    RESET_STORE,
    UPDATE_FLOW_STATUS,
    FREEZE_SCHOOL_INFO,
    ERROR_SCHOOL_INFO,
    FREEZE_SCHOOL_NAME,
    ERROR_SCHOOL_NAME,
    ALL_REQUESTED_USERS,
    USER_APPROVAL_SUCCESS,
    ERROR_ADD_TEACHERS,
    ROLES_RECEIVED,
    MODULES_RECEIVED,
    FEATURES_RECEIVED,
    FEATURE_REMOVED,
    ERROR_CREATE_FEATURE,
    SUCCESS_CREATE_FEATURE,
    FREEZE_CREATE_FEATURE,
    UPDATE_CREATE_FEATURE,
    PROFILES_RECEIVED,
    FREEZE_CREATE_PROFILE,
    SUCCESS_CREATE_PROFILE,
    ERROR_CREATE_PROFILE,
    PROFILE_REMOVED,
    SUBJECTS_RECEIVED,
    FREEZE_PROFILE_IMAGE_UPLOAD,
    TEACHER_PROFILE_IMAGE_ERROR,
    TEACHER_PROFILE_IMAGE_SUCCESS,
    FREEZE_FORGOT_PASSWORD,
    ERROR_FORGOT_PASSWORD,
    SUCCESS_FORGOT_PASSWORD,
    FREEZE_RESET_PASSWORD,
    ERROR_RESET_PASSWORD,
    SUCCESS_RESET_PASSWORD,
    FREEZE_SAVE_CSV,
    ERROR_CSV_DATA,
    SUCCESS_CSV_DATA,
    ENCRYPT_CSV_DATA,
    DATA_MENU_LIST,
    REDIRECT_LOGIN_PAGE,
    DATA_SUBJECTS,
    ERROR_SUBJECT,
    DATA_GRADES,
    ERROR_GRADES,
    UPDATE_SUCCESS,
    ERROR_UPDATE,
    FREEZE_STUDENTS_IMAGE_UPLOAD,
    STUDENTS_IMAGE_ERROR,
    STUDENTS_IMAGE_SUCCESS,
    GET_DATA_SUCCESS,
    ERROR_GET_STUDENT,
    GET_INITIAL_FORM_SUCESS,
    GET_INITIAL_FORM_ERROR,
    UPDATE_STUDENT_FORM,
    UPDATE_STUDENT_FORM_ERROR,
    GET_PARENT_PROFILE_INITIAL_FORM_SUCCESS,
    GET_PARENT_PROFILE_INITIAL_FORM_ERROR,
    REGISTER_PARENT_AND_STUDENT_PROFILE_SUCCESS,
    REGISTER_PARENT_AND_STUDENT_PROFILE_ERROR,
    ERROR_TEACHERS_LIST
} from '../actionTypes/index';


import {
    getRoles,
    postSchoolInfo,
    postSchoolName,
    getRequestedUsers,
    userApproved,
    createTeachersProfiles,
    getModules,
    getFeatures,
    removeFeature,
    createFeatureRequest,
    getProfiles,
    createProfileRequest,
    deleteProfileRequest,
    getSubjects,
    approveSignUp,
    uploadAndCropImage,
    requestForgotPassword,
    requestResetPassword,
    saveCsvRequest,
    getMenuList,
    getAllSubjects,
    getAllGrades,
    updateTeacherprofile,
    getAllStudent,
    getInitialForm,
    updateStudentFormData,
    getInitialValuesForParentAndStudentForm,
    registerParentAndStudentFormContent,
    createTeachersList
} from '../api/api';


export function* submitSchoolInfo({values}) {
    try {
        yield put({type: FREEZE_SCHOOL_INFO});
        const {error, ...response} = yield call(postSchoolInfo, values);
        if (error) {
            yield put({type: ERROR_SCHOOL_INFO, error});
        } else {
            yield put({type: UPDATE_FLOW_STATUS, flowStatus: response.flowstatus});
        }
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
        else
            yield put({type: ERROR_SCHOOL_INFO, error: error.message});
    }
}

export function* submitSchoolName({values}) {
    try {
        yield put({type: FREEZE_SCHOOL_NAME});
        const {error} = yield call(postSchoolName, values);
        if (error) {
            yield put({type: ERROR_SCHOOL_NAME, error});
        } else {
            yield put({type: UPDATE_FLOW_STATUS, flowStatus: 500});
        }
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
        else
            yield put({type: ERROR_SCHOOL_NAME, error: error.message});
    }
}

export function* fetchRequestedUsers() {
    const requestedUsers = yield call(getRequestedUsers);
    yield put({type: ALL_REQUESTED_USERS, requestedUsers});
}

export function* submitUserApproved({userID}) {
    yield call(userApproved, userID);
    yield put({type: USER_APPROVAL_SUCCESS, userID});
}

export function* addTeachers({values}) {
    try {
        const {error} = yield call(createTeachersProfiles, values);
        if (error) {
            yield put({type: ERROR_ADD_TEACHERS, error});
        } else {
            yield put({type: UPDATE_FLOW_STATUS, flowStatus: 600});
        }
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
        else
            yield put({type: ERROR_ADD_TEACHERS, error: error.message});
    }
}

export function* TeachersList({values}) {
    try {
        const {error} = yield call(createTeachersList, values);
        if (error) {
            yield put({type: ERROR_TEACHERS_LIST, error});
        } else {
            yield put({type: UPDATE_FLOW_STATUS, flowStatus: 1000});
                          // flowStatus 1000 is Menu Item for the timebeing we will change it later.
                           // for the time being we are trying to breakthe signUpflow logic
        }
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
        else
            yield put({type: ERROR_TEACHERS_LIST, error: error.message});
    }
}

export function* fetchRoles() {
    try {
        const roles = yield call(getRoles);
        yield put({type: ROLES_RECEIVED, roles});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}

export function* fetchModules() {
    try {
        const modules = yield call(getModules);
        yield put({type: MODULES_RECEIVED, modules});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}

export function* fetchFeatures() {
    try {
        const features = yield call(getFeatures);
        yield put({type: FEATURES_RECEIVED, features});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}

export function* deleteFeature({id}) {
    try {
        yield call(removeFeature, id);
        yield put({type: FEATURE_REMOVED, id});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}

export function* createFeature({values}) {
    try {
        yield put({type: FREEZE_CREATE_FEATURE});
        const {error, ...feature} = yield call(createFeatureRequest, values);
        if (error) {
            yield put({type: ERROR_CREATE_FEATURE, error});
        } else {
            if (values.id) {
                yield put({type: UPDATE_CREATE_FEATURE, values});
            } else {
                yield put({type: SUCCESS_CREATE_FEATURE, values: feature});
            }
        }
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
        else
            yield put({type: ERROR_CREATE_FEATURE, error: error.message});
    }
}

export function* fetchProfiles() {
    try {
        const profiles = yield call(getProfiles);
        yield put({type: PROFILES_RECEIVED, profiles});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}

export function* createProfile({values}) {
    try {
        yield put({type: FREEZE_CREATE_PROFILE});
        const {error, ...profile} = yield call(createProfileRequest, values);
        if (error) {
            yield put({type: ERROR_CREATE_PROFILE, error});
        } else {
            if (values.profileID) {
                yield put({type: SUCCESS_CREATE_PROFILE, values});
            } else {
                values.id = profile[0].id;
                values.profileID = profile[0].profileID;
                yield put({type: SUCCESS_CREATE_PROFILE, values});
            }
        }
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
        else
            yield put({type: ERROR_CREATE_PROFILE, error: error.message});
    }
}

export function* deleteProfile({profileID}) {
    try {
        yield call(deleteProfileRequest, profileID);
        yield put({type: PROFILE_REMOVED, profileID});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}

export function* fetchSubjects() {
    try {
        const subjects = yield call(getSubjects);
        yield put({type: SUBJECTS_RECEIVED, subjects});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}

export function* signUpApprove() {
    const {flowStatus} = yield call(approveSignUp);
    yield put({type: UPDATE_FLOW_STATUS, flowStatus});
}

export function* uploadProfilePic({file, crop}) {
    try {
        yield put({type: FREEZE_PROFILE_IMAGE_UPLOAD});
        const {error, ...data} = yield call(uploadAndCropImage, {file, crop});
        if (error) {
            yield put({type: TEACHER_PROFILE_IMAGE_ERROR, error});
        } else {
            yield put({type: TEACHER_PROFILE_IMAGE_SUCCESS, data});
        }
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}

export function* uploadStudentsPic({file, crop}) {
    try {
        yield put({type: FREEZE_STUDENTS_IMAGE_UPLOAD});
        const {error, ...data} = yield call(uploadAndCropImage, {file, crop});
        if (error) {
            yield put({type: STUDENTS_IMAGE_ERROR, error});
        } else {
            yield put({type: STUDENTS_IMAGE_SUCCESS, data});
        }
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
    }
}

export function* forgotPassword({values}) {
    yield put({type: FREEZE_FORGOT_PASSWORD});
    const {error, ...data} = yield call(requestForgotPassword, values);
    if (error) {
        yield put({type: ERROR_FORGOT_PASSWORD, error});
    } else {
        yield put({type: SUCCESS_FORGOT_PASSWORD, data});
    }
}

export function* resetPassword({values}) {
    yield put({type: FREEZE_RESET_PASSWORD});
    try {
        const {error} = yield call(requestResetPassword, values);
        if (error) {
            yield put({type: ERROR_RESET_PASSWORD, error});
        } else {
            yield put({type: SUCCESS_RESET_PASSWORD, success: true});
            yield call(delay, 1000);
            yield put({type: SUCCESS_RESET_PASSWORD, success: false});
            yield put({type: REDIRECT_LOGIN_PAGE, redirectedLogin: true});
        }
    } catch (error) {
        yield put({type: ERROR_RESET_PASSWORD, error: error.message});
    }
}

export function* saveCsvData({values}) {
    yield put({type: FREEZE_SAVE_CSV});
    try {
        yield call(checkStudentsCsv, values.csvFile, 'member');
        yield put({type: ENCRYPT_CSV_DATA});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
        else
            yield put({type: ERROR_CSV_DATA, error: error.message || error});
        return;
    }
    try {
        const {error} = yield call(saveCsvRequest, values);
        if (error) {
            yield put({type: ERROR_CSV_DATA, error});
        } else {
            yield put({type: SUCCESS_CSV_DATA, success: true});
        }
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE});
        else
            yield put({type: ERROR_CSV_DATA, error: error.message});
    }
}

export function* fetchMenuList() {
    try {
        const menuList = yield call(getMenuList);
        yield put({type: DATA_MENU_LIST, menuList});
    } catch (error) {
        if (error.message === 'Unauthorized')
            yield put({type: RESET_STORE, error});
    }
}

export function* getReadAllSubjects(values) {
    try {
        const subjects = yield call(getAllSubjects, values.data)
        yield put({type: DATA_SUBJECTS, subjects})
    } catch (error) {
        yield put({type: ERROR_SUBJECT, error})
    }
}

export function* getReadAllGradeLevels(values) {
    try {
        const gradeLevels = yield call(getAllGrades, values.data)
        yield put({type: DATA_GRADES, gradeLevels})
    } catch (error) {
        yield put({type: ERROR_GRADES, error})
    }
}

export function* updateTeacherProfile(data) {
    try {
        const updateResult = yield call(updateTeacherprofile, data.data)
        yield put({type: UPDATE_SUCCESS, updateResult})
    } catch (error) {
        yield put({type: ERROR_UPDATE, error})
    }
}

export function* getAllStudentList() {
    try {
        const dataResult = yield call(getAllStudent)
        yield put({type: GET_DATA_SUCCESS, dataResult})
    } catch (error) {
        yield put({type: ERROR_GET_STUDENT, error})
    }
}

export function* getStudentForm(data) {
    try {
        const initialForm = yield call(getInitialForm, {studentID: data.studentID})
        yield put({type: GET_INITIAL_FORM_SUCESS, initialForm})
    } catch (error) {
        yield put({type: GET_INITIAL_FORM_ERROR, error})
    }
}

export function* updateStudentForm(data) {
    try {
        const updateForm = yield call(updateStudentFormData, data.data)
        yield put({type: UPDATE_STUDENT_FORM, updateForm})
    } catch (error) {
        yield put({type: UPDATE_STUDENT_FORM_ERROR, error})
    }
}

export function* getParentAndStudentRegistrationForm() {
    try {
        const initialForm = yield call(getInitialValuesForParentAndStudentForm)
        yield put({type: GET_PARENT_PROFILE_INITIAL_FORM_SUCCESS, initialForm })
    } catch (error) {
        yield put({type: GET_PARENT_PROFILE_INITIAL_FORM_ERROR, error})
    }
}

export function* registerParentStudentProfile(data) {
    try {
        const registerResult = yield call(registerParentAndStudentFormContent, data.data);
        yield put({type: REGISTER_PARENT_AND_STUDENT_PROFILE_SUCCESS, registerResult})
    } catch (error) {
        yield put({type: REGISTER_PARENT_AND_STUDENT_PROFILE_ERROR, error})
    }
}
