import {
  ALL_REQUESTED_USERS,
  USER_APPROVAL_SUCCESS,
} from '../actionTypes/index';

export const initialState = {
  users: [],
};

const requestedUsers = (state = initialState, action) => {
  switch (action.type) {
    case ALL_REQUESTED_USERS:
      return {
        ...state,
        users: [...state.users, ...action.requestedUsers],
      };
    case USER_APPROVAL_SUCCESS:
      const users = state.users.filter(user => {
        return user.id !== action.userID
      });
      return {
        ...state,
        users,
      };
    default:
      return state;
  }
};

export default requestedUsers;
