import {USER_PROFILE, UPDATE_FLOW_STATUS} from '../actionTypes/index';

const signUp = (state = {}, action) => {
    switch (action.type) {
        case USER_PROFILE:
            return {...state, ...action.data};
        case UPDATE_FLOW_STATUS:
            return {...state, flowStatus: action.flowStatus}
        default:
            return state;
    }
};

export default signUp;
