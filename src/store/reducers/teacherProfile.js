import {
    IMAGE_CHANGE,
    CROP_CHANGE,
    TEACHER_PROFILE_IMAGE_ERROR,
    TEACHER_PROFILE_IMAGE_SUCCESS,
    TOGGLE_CROPPER,
    FREEZE_PROFILE_IMAGE_UPLOAD,
    DATA_SUBJECTS,

    DATA_GRADES,

    UPDATE_SUCCESS,

} from '../actionTypes/index';

const initialState = {
    profilePicCrop: '/images/default.png',
    profilePic: '',
    file: '',
    crop: {
        x: 25,
        y: 25,
        aspect: 1,
    },
    imageError: '',
    openCropper: false,
    freezeCropper: false,
    subjects: [],
    gradeLevels: [],
    updateResult: ''
};

const teacherProfile = (state = initialState, action) => {
    switch (action.type) {
        case IMAGE_CHANGE:
            return {...state, profilePic: action.profilePic, file: action.file, openCropper: true};
        case TOGGLE_CROPPER:
            return {...state, openCropper: !state.openCropper, imageError: ''};
        case CROP_CHANGE:
            return {...state, crop: {...state.crop, ...action.crop}};
        case TEACHER_PROFILE_IMAGE_ERROR:
            return {...state, imageError: action.imageError, freezeCropper: false, openDialog: false}
        case TEACHER_PROFILE_IMAGE_SUCCESS:
            return {...state, ...action.data, freezeCropper: false, openCropper: false}
        case FREEZE_PROFILE_IMAGE_UPLOAD:
            return {...state, freezeCropper: true};
        case DATA_SUBJECTS:
            return {...state, subjects: action.subjects}
        case DATA_GRADES:
            return {...state, gradeLevels: action.gradeLevels}
        case UPDATE_SUCCESS:
            return {...state, updateResult: action.updateResult}

        default:
            return state;
    }
};

export default teacherProfile;
