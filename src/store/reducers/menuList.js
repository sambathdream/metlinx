import {
  DATA_MENU_LIST,
  RESET_STORE,
  LOG_OUT_MESSAGE
} from '../actionTypes/index';

export const initialState = {
  menuList: {},
  error: '',
  logoutMessage: false
};

const menuList = (state = initialState, action) => {
  switch (action.type) {
    case DATA_MENU_LIST:
      return {...state, menuList: {...state.menuList, ...action.menuList}};
    case RESET_STORE:
      return {...state, error: action.error, logoutMessage: false};
    case LOG_OUT_MESSAGE:
      return {...state, logoutMessage: true };
    default:
      return state;
  }
};

export default menuList;
