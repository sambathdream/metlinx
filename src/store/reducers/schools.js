import {
  RECEIVE_SCHOOLS,
  SCHOOLS_FILTER,
  PAGE_CHANGE,
  FORM_OPEN,
} from '../actionTypes/index';

export const initialState = {
  schools: [],
  filter: 'EL',
  page: 1,
  form: {
    open: false,
    schoolID: '',
  }
};

const features = (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_SCHOOLS:
      return { ...state, schools: [ ...state.schools, ...action.schools ] };
    case SCHOOLS_FILTER:
      return { ...state, filter: action.filter, page: 1 };
    case PAGE_CHANGE:
      return { ...state, page: action.page };
    case FORM_OPEN:
      return { ...state, form: {
         ...state.form,
         open: action.open,
         schoolID: action.schoolID,
        }
      };
    default:
      return state;
  }
};

export default features;
