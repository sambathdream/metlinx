import {
    SIDEMENU_ACTION_CLOSE

} from '../actionTypes/index';

export const initialState = {
    freeze: false,
    error: '',
    success: false,
};

const sideMenuActionClose = (state = initialState, action) => {
    switch (action.type) {
        case SIDEMENU_ACTION_CLOSE:
            return {...state, success: true};
        default:
            return state;
    }
};

export default sideMenuActionClose;
