import {
  SIDEBAR,
} from '../actionTypes/index';

export const initialState = {
  menu: true,
};

const sidebar = (state = initialState, action) => {
  switch (action.type) {
    case SIDEBAR:
      return {...state, menu: action.open};
    default:
      return state;
  }
};

export default sidebar;
