import {
    FREEZE_LOGIN,
    SUCCESS_LOGIN,
    ERROR_LOGIN
} from '../actionTypes/index';

export const initialState = {
    freeze: false,
    error: '',
    success: false,
};

const login = (state = initialState, action) => {
    switch (action.type) {
        case FREEZE_LOGIN:
            return {...state, freeze: true};
        case ERROR_LOGIN:
            return {...state, error: 'Log in failed', freeze: false};
        case SUCCESS_LOGIN:
            return {...state, success: true};
        default:
            return state;
    }
};

export default login;
