import {
    TEACHER_ROLE_CLICKED,
    STUDENT_ROLE_CLICKED,
    PARENT_ROLE_CLICKED,
  COUNTIES_DISTRICT_LIST,
  STATES_LIST,
  FREEZE_SCHOOL_INFO,
  ERROR_SCHOOL_INFO,
  FREEZE_SIGN_UP,
  SUCCESS_SIGN_UP,
  ERROR_SIGN_UP
} from '../actionTypes/index';


export const initialState = {
  username:'',
  password:'',
  states: [],
  districts: [],
  counties: [],
  freeze: false,
  error: '',
  errorMessage:'',
  success: false,

};

const schoolRegistration = (state = initialState, action) => {


  switch (action.type) {
      case STATES_LIST:
          return {
              ...state,
              states: [...state.states, ...action.states],
          };
      case COUNTIES_DISTRICT_LIST:
          return {
              ...state,
              districts: [...action.districts],
              counties: [...action.counties],
          };
      case FREEZE_SIGN_UP:
          return {...state, freeze: true};
      case ERROR_SIGN_UP:
          return {...state, error: action.error, freeze: false};
      case SUCCESS_SIGN_UP:
          return {...state, success: true};

    case FREEZE_SCHOOL_INFO:
      return {
        ...state,
        freeze: true,
        error: '',
      };
    case ERROR_SCHOOL_INFO:
      return {
        ...state,
        freeze: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default schoolRegistration;
