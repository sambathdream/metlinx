import {
    GET_TEACHER_LIST_INITIAL_FORM_SUCCESS,
    SAVE_TEACHER_LIST,
    ERROR_TEACHERS_LIST,
    FREEZE_TEACHERS_LIST,
    CREATE_TEACHERS_LIST,
    SUCCESS_TEACHERS_LIST,
    CLOSE_TL_DIALOG

} from '../actionTypes/index';

const initialState = {
    initialForm: {},
    TeacherListFormData: {},
    error: '',
    TLDialog: false,
    success: false,
    freeze: false
};


const TeachersList = (state = initialState, action) => {
    switch (action.type) {
        case GET_TEACHER_LIST_INITIAL_FORM_SUCCESS:
            return {...state, initialForm: action.initialForm};
        case SAVE_TEACHER_LIST:
            return {...state, TeacherListFormData: action.data };
        case FREEZE_TEACHERS_LIST:
            return {...state, freeze: true, TLDialog: true};
        case CREATE_TEACHERS_LIST:
            return {...state, TeacherListFormData: action.data };
        case ERROR_TEACHERS_LIST:
            return {...state, error: action.error, freeze: false};
        case SUCCESS_TEACHERS_LIST:
            return {...state, error: '', freeze: false, success: true};
        case CLOSE_TL_DIALOG:
            return {...state, TLDialog: false, error: '', freeze: false, success: false};

        default:
            return state;
    }
};

export default TeachersList;
