import {
    STUDENTS_IMAGE_ERROR,

    STUDENTS_CROP_CHANGE,
    STUDENTS_TOGGLE_CROPPER,
    STUDENTS_IMAGE_CHANGE,
    FREEZE_STUDENTS_IMAGE_UPLOAD,
    GET_DATA_SUCCESS,

    GET_INITIAL_FORM_SUCESS,
    STUDENT_FORM,
    HANDLE_CLOSE_FORM,
    UPDATE_STUDENT_FORM,
    OPEN_STUDENT_FORM,
    STUDENTS_IMAGE_SUCCESS
} from '../actionTypes/index';

const initialState = {
    studentsPicCrop: '/images/default.png',
    studentsPic: '',
    file: '',
    crop: {
        x: 25,
        y: 25,
        aspect: 1,
    },
    imageError: '',
    openCropper: false,
    freezeCropper: false,
    studentLists: [],
    form: {
        open: false
    },
    initialForm: {}
};

const students = (state = initialState, action) => {
    switch (action.type) {
        case STUDENTS_IMAGE_CHANGE:
            return { ...state, studentsPic: action.studentPic, file: action.file, openCropper: true };
        case STUDENTS_TOGGLE_CROPPER:
            return { ...state, openCropper: !state.openCropper, imageError: ''};
        case STUDENTS_CROP_CHANGE:
            return { ...state, crop: {...state.crop, ...action.crop} };
        case STUDENTS_IMAGE_ERROR:
            return { ...state, imageError: action.imageError, freezeCropper: false, openDialog: false }
        case FREEZE_STUDENTS_IMAGE_UPLOAD:
            return { ...state, freezeCropper: true };
        case GET_DATA_SUCCESS:
            return { ...state, studentLists: action.dataResult };
        case STUDENT_FORM:
            return {
                ...state,
                form: {
                    ...state.form,
                    open: action.open
                }
            };
        case GET_INITIAL_FORM_SUCESS:
            return {
                ...state,
                initialForm: action.initialForm
            };
        case HANDLE_CLOSE_FORM:
            return {
                ...state,
                form: {
                    ...state.form,
                    open: false
                }
            };
        case UPDATE_STUDENT_FORM:
            return {
                ...state,
                form: {
                    ...state.form,
                    open: false
                }
            };
        case OPEN_STUDENT_FORM:
            return {
                ...state,
                form: {
                    ...state.form,
                    open: action.open
                }
            };
        case STUDENTS_IMAGE_SUCCESS:
            return {
                ...state,
                studentsPic: action.data.profilePic,
                studentsPicCrop: action.data.profilePicCrop,
                freezeCropper: false,
                openCropper: false
            };
        // case DATA_SUBJECTS:
        //     return { ...state, subjects: action.subjects }
        // case DATA_GRADES:
        //     return { ...state, gradeLevels: action.gradeLevels }
        // case UPDATE_SUCCESS:
        //     return { ...state, updateResult: action.updateResult }

        default:
            return state;
    }
};

export default students;