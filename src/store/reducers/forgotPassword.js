import {
  FREEZE_FORGOT_PASSWORD,
  ERROR_FORGOT_PASSWORD,
  SUCCESS_FORGOT_PASSWORD,
} from '../actionTypes';

export const initialState = {
  freeze: false,
  error: '',
  success: false,
};

const forgotPassword = (state = initialState, action) => {
  switch (action.type) {
    case FREEZE_FORGOT_PASSWORD:
      return {...state, freeze: true};
    case ERROR_FORGOT_PASSWORD:
      return {...state, error: action.error, freeze: false};
    case SUCCESS_FORGOT_PASSWORD:
      return {...state, success: true};
    default:
      return state;
  }
};

export default forgotPassword;
