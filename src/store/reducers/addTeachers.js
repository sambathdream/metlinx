import {
  ERROR_ADD_TEACHERS,
  FREEZE_ADD_TEACHERS,
} from '../actionTypes';

export const initialState = {
  error: '',
  freeze: false,
};

const addTeachers = (state = initialState, action) => {
  switch (action.type) {
    case ERROR_ADD_TEACHERS:
      return {
        ...state,
        error: action.error,
        freeze: false,
      };
    case FREEZE_ADD_TEACHERS:
      return {
        ...state,
        freeze: action.freeze,
        error: '',
      };
    default:
      return state;
  }
};

export default addTeachers;
