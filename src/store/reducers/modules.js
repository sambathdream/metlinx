import {
  MODULES_RECEIVED,
} from '../actionTypes/index';

export const initialState = {
  modules: [],
};

const modules = (state = initialState, action) => {
  switch (action.type) {
    case MODULES_RECEIVED:
      return {...state, modules: [...state.modules, ...action.modules]};
    default:
      return state;
  }
};

export default modules;
