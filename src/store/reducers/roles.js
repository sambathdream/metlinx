import {
  ROLES_RECEIVED,
} from '../actionTypes/index';

export const initialState = {
  roles: [],
};

const roles = (state = initialState, action) => {
  switch (action.type) {
    case ROLES_RECEIVED:
      return {...state, roles: [...state.roles, ...action.roles]};
    default:
      return state;
  }
};

export default roles;
