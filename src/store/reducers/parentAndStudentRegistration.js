import {
    GET_PARENT_PROFILE_INITIAL_FORM_SUCCESS,
    SAVE_FORM_DATA
} from '../actionTypes/index';

const initialState = {
    initialForm: {},
    parentFormData: {},
    studentFormData: {}

};

const parentAndStudentRegistration = (state = initialState, action) => {
    switch (action.type) {
        case GET_PARENT_PROFILE_INITIAL_FORM_SUCCESS:
            return {...state, initialForm: action.initialForm};
        case SAVE_FORM_DATA:
            if(action.step === 0) {
                return {...state, parentFormData: action.data && action.data.ParentRegistration.values}
            }
            if(action.step === 1) {
                return {...state, studentFormData: action.data && action.data.StudentRegistration.values}
            };
        default:
            return state;
    }
};

export default parentAndStudentRegistration;