import {
  CSV_LEVEL,
  FREEZE_SAVE_CSV,
  ERROR_CSV_DATA,
  SUCCESS_CSV_DATA,
  ENCRYPT_CSV_DATA,
  CLOSE_CSV_DIALOG,
} from '../actionTypes';

export const initialState = {
  csvLevel: '',
  freeze: false,
  csvDialog: false,
  encryptCsv: false,
  error: '',
  success: false,
};

const importCsv = (state = initialState, action) => {
  switch (action.type) {
    case CSV_LEVEL:
      return {...state, csvLevel: action.value};
    case FREEZE_SAVE_CSV:
      return {...state, freeze: true, csvDialog: true};
    case ENCRYPT_CSV_DATA:
      return {...state, encryptCsv: true};
    case ERROR_CSV_DATA:
      return {...state, error: action.error, freeze: false}
    case SUCCESS_CSV_DATA:
      return {...state, error: '', freeze: false, success: true}
    case CLOSE_CSV_DIALOG:
      return {...state, csvDialog: false, encryptCsv: false, error: '', freeze: false, success: false}
    default:
      return state;
  }
};

export default importCsv;
