import {
  FREEZE_RESET_PASSWORD,
  ERROR_RESET_PASSWORD,
  SUCCESS_RESET_PASSWORD,
  REDIRECT_LOGIN_PAGE
} from '../actionTypes/index';

export const initialState = {
  freeze: false,
  error: '',
  success: false,
  redirectedLogin: false
};

const resetPassword = (state = initialState, action) => {
  switch (action.type) {
    case FREEZE_RESET_PASSWORD:
      return {...state, freeze: true};
    case ERROR_RESET_PASSWORD:
      return {...state, error: action.error, freeze: false};
    case SUCCESS_RESET_PASSWORD:
      return {...state, success: action.success, freeze: false, };
    case REDIRECT_LOGIN_PAGE:
      return {...state, redirectedLogin: action.redirectedLogin};
    default:
      return state;
  }
};

export default resetPassword;
