import {
  FREEZE_SCHOOL_NAME,
  ERROR_SCHOOL_NAME,
} from '../actionTypes/index';

export const initialState = {
  freeze: false,
  error: '',
};

const schoolName = (state = initialState, action) => {
  switch (action.type) {
    case FREEZE_SCHOOL_NAME:
      return {...state, freeze: true};
    case ERROR_SCHOOL_NAME:
      return {...state, error: action.error, freeze: false};
    default:
      return state;
  }
};

export default schoolName;
