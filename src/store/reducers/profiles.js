import {
  PROFILES_RECEIVED,
  PROFILE_FORM,
  FREEZE_CREATE_PROFILE,
  SUCCESS_CREATE_PROFILE,
  ERROR_CREATE_PROFILE,
  PROFILE_REMOVED,
  PROFILE_FILTER_TEXT,
  PROFILE_FILTER,
} from '../actionTypes/index';

export const initialState = {
  profiles: {},
  form: {
    open: false,
    profileID: 'new',
  },
  freeze: false,
  filter: {
    enabled: false,
    filterText: '',
  },
};

function removeByKey (myObj, deleteKey) {
  return Object.keys(myObj)
    .filter(key => parseInt(key, 10) !== deleteKey)
    .reduce((result, current) => {
      result[current] = myObj[current];
      return result;
    }, {});
}

const profiles = (state = initialState, action) => {
  switch (action.type) {
    case PROFILES_RECEIVED:
      return { ...state, profiles: action.profiles };
    case PROFILE_FORM:
      return {
        ...state,
        form: {
          ...state.form,
          open: action.open,
          profileID: action.profileID,
        },
        error: '',
      };
    case SUCCESS_CREATE_PROFILE:
      return {
        ...state,
        profiles: {...state.profiles, [action.values.profileID]: action.values},
        freeze:  false,
        error: '',
        form: {...state.form, open: false},
      };
    case FREEZE_CREATE_PROFILE:
      return {
        ...state,
        freeze: true,
        error: '',
      };
    case ERROR_CREATE_PROFILE:
      return {
        ...state,
        freeze: false,
        error: action.error,
      };
    case PROFILE_REMOVED:
      return {
        ...state,
        profiles: removeByKey(state.profiles, action.profileID),
      };
    case PROFILE_FILTER:
      return {
        ...state,
        filter: {
          ...state.filter,
          enabled: action.enabled,
        }
      };
    case PROFILE_FILTER_TEXT:
      return {
        ...state,
        filter: {
          ...state.filter,
          filterText: action.text,
        }
      };
    default:
      return state;
  }
};

export default profiles;
