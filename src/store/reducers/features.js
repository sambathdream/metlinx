import {
  FEATURES_RECEIVED,
  FEATURE_REMOVED,
  FEATURE_FORM,
  ERROR_CREATE_FEATURE,
  SUCCESS_CREATE_FEATURE,
  FREEZE_CREATE_FEATURE,
  UPDATE_CREATE_FEATURE,
  FEATURE_FILTER,
  FILTER_MODULEID,
} from '../actionTypes';

export const initialState = {
  features: [],
  form: {
    open: false,
    id: 'new',
  },
  error: '',
  freeze: false,
  filter: {
    enabled: false,
    moduleID: 'PARENT',
  },
};

const features = (state = initialState, action) => {
  switch (action.type) {
    case FEATURES_RECEIVED:
      return {...state, features: [...state.features, ...action.features], error: ''};
    case FEATURE_REMOVED:
      return {
        ...state,
        features: state.features.filter(feature => {
          if (feature.id === action.id) {
            return false;
          }
          return true;
        }),
        error: '',
      };
    case FEATURE_FORM:
      return {
        ...state,
        form: {
          ...state.form,
          open: action.open,
          id: action.id,
        },
        error: '',
      }
    case SUCCESS_CREATE_FEATURE:
      return {
        ...state,
        features: [...state.features, action.values],
        error: '',
        form: {...state.form, open: false},
        freeze: false,
      }
    case UPDATE_CREATE_FEATURE:
      return {
        ...state,
        features: state.features.map(feature => {
          if (feature.id === action.values.id) {
            return action.values;
          }
          return feature;
        }),
        form: {...state.form, open: false},
        error: '',
        freeze: false,
      };
    case ERROR_CREATE_FEATURE:
      return {
        ...state,
        error: action.error,
        freeze: false,
      }
    case FREEZE_CREATE_FEATURE:
      return {
        ...state,
        freeze: true,
      }
    case FEATURE_FILTER:
      return {
        ...state,
        filter: {
          ...state.filter,
          enabled: action.enabled,
        },
      };
    case FILTER_MODULEID:
      return {
        ...state,
        filter: {
          ...state.filter,
          moduleID: action.moduleID,
        },
      };
    default:
      return state;
  }
};

export default features;
