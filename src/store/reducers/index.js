import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';
import schools from './schools';
import teacherSignUp from './teacherSignUp';
import login from './userAuth';
import profile from './profile';
import schoolRegistration from './schoolRegistration';
import schoolName from './schoolName';
import requestedUsers from './requestedUsers';
import addTeachers from './addTeachers';
import sidebar from './sidebar';
import roles from './roles';
import modules from './modules';
import features from './features';
import profiles from './profiles';
import teacherProfile from './teacherProfile';
import forgotPassword from './forgotPassword';
import resetPassword from './resetPassword';
import importCsv from './importCsv';
import menuList from './menuList';
import students from './students';
import parentAndStudentRegistration from './parentAndStudentRegistration';
import TeachersList from './TeachersList';
import {RESET_STORE} from '../actionTypes/index';

const allReducers = combineReducers({
    form,
    login,
    schools,
    teacherSignUp,
    profile,
    schoolRegistration,
    schoolName,
    requestedUsers,
    addTeachers,
    sidebar,
    roles,
    modules,
    features,
    profiles,
    teacherProfile,
    forgotPassword,
    resetPassword,
    importCsv,
    menuList,
    students,
    parentAndStudentRegistration,
    TeachersList
});

const rootReducer = (state, action) => {
    if (action.type === RESET_STORE) {
        state = undefined
    }
    return allReducers(state, action)
}

export default rootReducer;
